import paramiko
import time

def connectviassh(router_ip,router_username,router_password,command, startmarker=None,endmarker=None,port=22):

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(router_ip, 
                username=router_username, 
                password=router_password,
                look_for_keys=False, port=int(port), timeout=5 )
    channel=ssh.invoke_shell()
    output=channel.recv(9999)
    channel.send(command+'\n')
    time.sleep(1)
    output=channel.recv(9999)
    response = output.decode('utf-8').replace("WARNING: Password is set to default value","").replace("admin  >","").replace(command,"")
    startindex=-1
    endindex=-1
    if startmarker and endmarker:
        startindex = response.find(startmarker)
        endindex = response.find(endmarker)
    if startindex!= -1:
        response = response[startindex:]
    if endindex!= -1:
        response = response[:endindex]
    ssh.close()
    return response

# Run with scanservice
# sipproxy status, SIPPROXY SHOW REG, show qos stats, show cac


# Run as commands
# show stats, sipproxy, show qos,  
#print(connectviassh("127.0.0.1","admin","admin","show stats",port=5036))