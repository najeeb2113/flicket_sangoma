import json
import time
import warnings
from selenium import webdriver
import pprint
import configparser
import random
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.options import Options
import os
from orm import vega, session, deviceinfo
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


config = configparser.ConfigParser()
config.read('deviceconfig.ini')

# print(list(config.sections()))
# print(config.options(section='SANGOMAVEGA'))
#
# print(list(config['sangoma acid'].keys()))
# for key in config['sangoma acid']:
#     print(key)

chrome_options = Options()
chrome_options.add_argument("--headless")
# chrome_options.add_argument("--window-size=1920x1080")

chrome_options.add_argument("window-size=1200x600")

# download the chrome driver from https://sites.google.com/a/chromium.org/chromedriver/downloads and put it in the
# current directory
chrome_driver = os.getcwd() + "\\chromedriver.exe"

result = session.query(deviceinfo).all()
session.commit()

for data in result:
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
    try:
        driver.get(data.ip)
        for keys in list(config.sections()):
            if keys == data.device_type:
                driver.find_element_by_id(config[keys]['username_id']).send_keys(data.username)
                form = driver.find_element_by_id(config[keys]['password_id']).send_keys(data.password)
                driver.get_screenshot_as_file("captureLogin0123.png")
                driver.find_element_by_id(config[keys]['password_id']).send_keys(Keys.RETURN)
                mydata = []
                myvalues = []
                databasepush_dict = {}
                databasepush_values = []
                counter = 0

                if config[keys]['readparams']:
                    readparams = config[keys]['readparams']

                    mydata = readparams.split(',')
                    print("its my data ",mydata)
                    for index, item in enumerate(mydata):
                        # print(keys)
                        print(item, ":", config[keys][item])

                        myvalues = config[keys][item].split(',')
                        print("myvalues",myvalues)
                        url = myvalues[0]
                        valueid = myvalues[1]
                        print("before url")
                        driver.get(url)
                        print("after url")

                        time.sleep(5)
                        driver.get_screenshot_as_file("capture" + str(index) + ".png")
                        try:
                            tempdata = driver.find_element_by_id(valueid).tag_name
                            print('mytemp', tempdata)

                            if tempdata == "input":
                                inputelem = driver.find_element_by_id(valueid)
                                if inputelem == '':
                                    inputelem = "unable to fetch"
                                #     the url is unavailable the code is working fine
                                databasepush_values.append(inputelem.get_attribute("value"))
                            elif tempdata == "select":
                                selectelem = Select(driver.find_element_by_id(valueid))
                                if selectelem == '':
                                    selectelem = "unable to fetch"
                                databasepush_values.append(selectelem.first_selected_option.text)
                            else:
                                td = driver.find_element_by_id(valueid).text
                                if td == '':
                                    td = "unable to fetch"
                                databasepush_values.append(td)
                            # print(databasepush_values)
                            databasepush_dict[item] = databasepush_values[counter]
                            counter += 1
                        except:
                            pass

                print("below is my data dictionary for sending to database")
                json_object = json.dumps(databasepush_dict, indent=0)
                ins = vega(device_name=data.device_name,device_type=data.device_type,paramvalue=json_object,paramname=str(mydata))
                session.add(ins)
                session.commit()
    except:
        pass
    print("Partially done!")
    driver.get_screenshot_as_file("afterloginimg"+str(random.randint(0,22))+".png")
    driver.implicitly_wait(3)
    driver.quit()
print("completely done")
# time.sleep(15)
# exec(open("./chromeheadless.py").read())
