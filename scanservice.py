import json
import time
import warnings
import pprint
import os
from orm import vega, session, deviceinfo
from selenium import webdriver
import configparser
from selenium.webdriver.chrome.service import Service
import random
from selenium.webdriver.chrome.options import Options
import os
import license
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from sshclient import connectviassh
import re
import redis
import pickle
config = configparser.ConfigParser()
config.read('deviceconfig.ini')
pattern = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
sysconfig = configparser.ConfigParser()
sysconfig.read("config.ini")
license.licenseCheck()
chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument("window-size=1200x600")
ser = Service(os.getcwd() + sysconfig["DRIVERPATH"]["CHROMEPATH"])
chrome_driver = os.getcwd() + sysconfig["DRIVERPATH"]["CHROMEPATH"]
snmpagentenable = False
try:
    snmpagentenable = bool(sysconfig["SNMPAGENT"]["ENABLE"])
except:
    snmpagentenable = False
    print("SNMPAGENT not configured or incorrect config. Setting it to OFF")

result = session.query(deviceinfo).all()
session.commit()

r = None
if snmpagentenable:
    if sysconfig['REDIS']['PASSWORD']!="":
        r = redis.Redis(host=sysconfig['REDIS']['HOST'],
    port=int(sysconfig['REDIS']['PORT']), password=sysconfig['REDIS']['PASSWORD'])
    else:
        r = redis.Redis(
    host=sysconfig['REDIS']['HOST'],
    port=int(sysconfig['REDIS']['PORT']))

for data in result:
    if(data.device_name.startswith("dummy")):
        print("Skipping Dummy node")
        continue
    if data.device_type not in list(config.sections()):
        print("Could not find config section in config file for :" +
              data.device_type+".. Skipping ...")
        continue
    driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver.set_page_load_timeout(float(sysconfig["DRIVERPATH"]["TIMEOUT"]))
    #driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
    try:
        webconnectsuccess = True
        sshconnectsuccess = True
        databasepush_dict = {}
        databasepush_values = []  
        counter = 0
        if config[data.device_type]['readparams']:
            try:
                driver.get(data.ip)
                driver.find_element(By.XPATH, config[data.device_type]['username_id']).send_keys(data.username)
                form = driver.find_element(By.XPATH, config[data.device_type]['password_id']).send_keys(data.password)
                driver.find_element(By.XPATH, config[data.device_type]['password_id']).send_keys(Keys.RETURN)              
                readparams = config[data.device_type]['readparams']
                readparams = readparams.split(',')
                print("ReadParams: ", readparams)
                for index, item in enumerate(readparams):
                    print(item, ":", config[data.device_type][item])
                    readparam = config[data.device_type][item].split(',')
                    print("Getting data for: ", data.ip + readparam[0])
                    
                    # driver.get_screenshot_as_file("capture.png")
                    xpval = ""
                    try:
                        url = data.ip + readparam[0]
                        driver.get(url)
                        time.sleep(int(config[data.device_type]["delay"]))
                        xpelement = driver.find_element(By.XPATH, readparam[1])
                        print("XPELEMENT", xpelement)
                        
                        if readparam[2] == 'text':
                            xpval = xpelement.text
                            if readparam[3] == "replace":
                                xpval = xpval.replace(' ', ',')
                            print("Text Value: ", xpval)
                        else:
                            xpval = xpelement.get_attribute("value")
                            print("Attribute Value: ", xpval)
                        
                    except Exception as aex:
                        print("Cannot Read Service for IP:" +
                            data.ip+" and Param:"+item)
                        print(aex)
                    databasepush_values.append(xpval)
                    databasepush_dict[item] = databasepush_values[counter]
                    counter += 1
            except Exception as ex:
                print("Could not connect to Device Web portal")
                print(ex)
                readparams = config[data.device_type]['readparams']
                readparams = readparams.split(',')
                for index, item in enumerate(readparams):
                    readparam = config[data.device_type][item].split(',')
                    databasepush_dict[item]="ERR"
                    webconnectsuccess = False


        #SSH based data read
        if config[data.device_type]['sshreadparams']:
            sshreadparams = config[data.device_type]['sshreadparams']
            sshreadparams = sshreadparams.split(',')
            print("SSHReadParams: ", sshreadparams)
            for index, item in enumerate(sshreadparams):
                print(item, ":", config[data.device_type][item])
                readparam = config[data.device_type][item].split(',')
                print("SSH Getting data for: ", data.ip + readparam[0])
                
                try:
                    #print(pattern.search(data.ip)[0],data.username,data.password, readparam[0],readparam[1],readparam[2])
                    ipaddress = str(data.ip).replace('http://','')
                    ipaddress = ipaddress.replace('/','')
                    #pattern.search(data.ip)[0]
                    print("Connecting on SSH to IP: ",ipaddress)
                    xpval = connectviassh(ipaddress, data.username, data.password, readparam[0],readparam[1],readparam[2])
                    databasepush_values.append(xpval)
                    databasepush_dict[item] = databasepush_values[counter]
                    counter += 1
                    print("Response: "+xpval)
                except Exception as aex:
                    print("Cannot Read SSH Service for IP:" +
                          data.ip+" and Param:"+item)
                    print(aex)   
                    sshconnectsuccess = False     
        if sshconnectsuccess or webconnectsuccess:
            json_object = json.dumps(databasepush_dict, indent=0)
            readparams.extend(sshreadparams)
            pprint.pprint(readparams)
            ins = vega(device_name=data.device_name, device_type=data.device_type,
                    paramvalue=json_object, paramname=str(readparams))
            session.add(ins)
            session.commit()
        else:
            if snmpagentenable and r is not None:
                r.publish(channel="outgoingsnmpagent", message = pickle.dumps(["unreachablenode"]))
                print("Sending unreachablenode to snmpagent")

            print("DB write cancelled. SSH and WEb connect both failed for ", data.ip)
    except Exception as erm:
        print("Exception detected for IP: "+data.ip +
              ". Check config file. Message: "+str(erm))
        print(erm)
        

    driver.implicitly_wait(3)
    driver.quit()
    if snmpagentenable and r is not None:
        r.close()
print("Ending service")
