from Cryptodome.Cipher import AES
# import uuid
from dateutil import parser
import sys
import time
import datetime
import os
import platform
from subprocess import Popen, PIPE


def getnode():
    if platform.system() == "Linux":
        process = Popen(["cat", "/var/lib/dbus/machine-id"],
                        stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        return stdout.strip().decode('utf-8')
    elif platform.system() == "Windows":
        process = Popen(["wmic", "csproduct", "get", "UUID"],
                        stdout=PIPE, stderr=PIPE)
        stdout, stderr = process.communicate()
        return stdout.strip().split()[1].decode('utf-8')

    else:
        print("Unsupported OS: "+str(platform.system()))
        sys.exit(255)


def licenseCheck():
    key = b'ABVF9071FGHGRTBT'

    try:
        licfile = open(os.path.join("", "license.bin"), "rb")
        nonce, tag, ciphertext = [licfile.read(x) for x in (16, 16, -1)]
        licfile.close()
    except:
        print("License File Not found. Please contact Telenext Systems to generate a new license")
        print("Unique System ID: " + str(getnode()))
        time.sleep(30)
        sys.exit(255)

    cipher = AES.new(key, AES.MODE_EAX, nonce=nonce)
    plaintext = cipher.decrypt(ciphertext)
    try:
        cipher.verify(tag)
        ddp = str(plaintext).split('#')
        node = ddp[2]
        expdate = ddp[3]
        numdevices = ddp[4]
        currentdate = datetime.datetime.now()
        expd = parser.parse(expdate)
        print(expd)
        if node == str(getnode()):
            if expd > currentdate:
                print("License File Valid. License expiry:" + str(expd))
                return numdevices
            else:
                print(
                    "License has expired. Please get a new license file. NodeID:" + str(getnode()))
                time.sleep(30)
                sys.exit(255)
        else:
            print(node)
            print("The License file does NOT belong to this machine. Please get a new license file. NodeID:" + str(
                getnode()))
            time.sleep(300)
            sys.exit(255)

    except ValueError:
        print("Key incorrect or message corrupted")
