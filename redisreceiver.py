import redis
import pickle
r = redis.Redis(
host='redis-10612.c264.ap-south-1-1.ec2.cloud.redislabs.com',
port=10612, password='spring')

pubsub = r.pubsub()
pubsub.subscribe(['snmptraps'])
for item in pubsub.listen():
    print(item)
    if item["type"] == "message":
        print(pickle.loads(item["data"]))