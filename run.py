swversion="v2.7.3"
swdate="12 June 2022"
import base64
# from crypt import methods
import importlib
from ipaddress import ip_address
import json
import pprint
import random
import time
import uuid
from datetime import datetime, timedelta
from subprocess import CalledProcessError
from os.path import isfile, join
import bitmath
from bitmath import *
import pickle
import flask
import requests.sessions
from selenium.webdriver.common.by import By
from sqlalchemy.exc import IntegrityError
from flask_babel import gettext
from application.flicket.models.flicket_user import FlicketUser, FlicketGroup
from application.flicket.scripts.flicket_config import set_flicket_config
from application.flicket.scripts.hash_password import hash_password
from application.flicket_admin.models.flicket_config import FlicketConfig
from application import db as db2
from application import __version__
from dbbackup import backup
from selenium import webdriver
from flask import Flask, request, redirect, render_template, url_for, flash, jsonify
from sqlalchemy.orm.exc import UnmappedInstanceError, FlushError
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import license
import sys
from moscalculator import getmosscore
import redis
from flask_login import (
    login_required,
    login_user,
    logout_user,
    current_user,
    LoginManager
)
from werkzeug.utils import secure_filename

from orm import session, Subscriber, vega, deviceinfo, cdrParser, Devicebackups, DeviceLicenses, monitoringNodes, \
    mosScores, Triggers, Alarms, Auditlogs, snmpRules, snmpAlerts
from waitress import serve
from sqlalchemy import delete, false, func

from passlib.hash import pbkdf2_sha256
import configparser

import os

from flask import abort
from flask import Flask
from flask import g
from flask import request
from flask_login import LoginManager
from flask_mail import Mail
from flask_pagedown import PageDown
from flask_sqlalchemy import SQLAlchemy
from flask_babel import Babel
from flaskext.markdown import Markdown

from application.flicket_admin.views import admin_bp
from application.flicket_api.views import bp_api
from application.flicket_errors import bp_errors
from application.flicket.views import flicket_bp
from application.flicket.scripts.jinja2_functions import now_year
from dash_application import create_dash_application
from dash_application.mos_graphs import create_dash_application2
import flickconfig
from sshclient import connectviassh
import re
import glob, os
from pythonping import ping
import speedtest
import runpy
import geocoder
pattern = re.compile(r'(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})')
licnumdev = 0
template_dir = os.path.abspath('./templates')
static_dir = os.path.abspath('./static')
app = Flask(__name__,template_folder=template_dir, static_folder=static_dir)
app.config.from_object('flickconfig.BaseConfiguration')
#app.config.update(TEMPLATES_AUTO_RELOAD=True)

db = SQLAlchemy(app)
mail = Mail(app)
pagedown = PageDown(app)

babel = Babel(app)
Markdown(app)
import sqlite3
from sqlite3 import Error
# import jinja function
app.jinja_env.globals.update(now_year=now_year)

# import models so alembic can see them
# noinspection PyPep8
from application.flicket.models import flicket_user, flicket_models
# noinspection PyPep8
from application.flicket_admin.models import flicket_config

# lm = LoginManager()
# lm.init_app(app)
# lm.login_view = 'flicket_bp.login'
from firmwareupdateroutes import fu_bp

app.register_blueprint(admin_bp)
app.register_blueprint(flicket_bp)
app.register_blueprint(bp_api)
app.register_blueprint(bp_errors)
app.register_blueprint(fu_bp)


devicedata = session.query(vega).first()
session.commit()
adminexist = session.query(Subscriber).all()
session.commit()

if len(adminexist) == 0:
    fullname = "AdminGuy"
    region = "None"
    email = "admin@ems.com"
    mobile = "1234567890"
    password = "Admin@123"
    isadmin = "true"
    readonly = "false"
    passhash = pbkdf2_sha256.hash(password)
    job_title = "admin"
    new_user = Subscriber(fullname=fullname, email=email,password=passhash, mobile=mobile, region=region, readonly=readonly, isadmin=isadmin)
    session.add(new_user)
    session.commit()

if not devicedata:
    print("no data in devicedata")
    # import runpy
    # file_globals = runpy.run_path("./scanservice", run_name='__main__')
    # print("from globals: ", file_globals.get('status'))
    # print("from globals: ", file_globals.get('myvar'))

@app.teardown_appcontext
def shutdown_session(exception=None):
    session.commit()
    db2.session.commit()
    print("remove db session")
    db2.session.remove()
    db.session.remove()
    session.remove()


@babel.localeselector
def get_locale():
    # if a user is logged in, use the locale from the user settings
    user = getattr(g, 'user', None)
    if hasattr(user, 'locale'):
        return user.locale
    # otherwise try to guess the language from the user accept
    # header the browser transmits.  We support de/fr/en in this
    # example.  The best match wins.
    return request.accept_languages.best_match(app.config['SUPPORTED_LANGUAGES'].keys())


@app.url_defaults
def set_language_code(endpoint, values):
    if 'lang_code' in values or not g.get('lang_code', None):
        return
    if app.url_map.is_endpoint_expecting(endpoint, 'lang_code'):
        values['lang_code'] = g.lang_code


@app.url_value_preprocessor
def get_lang_code(endpoint, values):
    if values is not None:
        g.lang_code = values.pop('lang_code', None)


@app.before_request
def ensure_lang_support():
    lang_code = g.get('lang_code', None)
    if lang_code and lang_code not in app.config['SUPPORTED_LANGUAGES'].keys():
        return abort(404)


#  *&^$#$%#%^&$____------ ABOVE FOR FLICKET , DASH   ________________----------

UPLOAD_FOLDER = 'tempfiles/'

app.config['SECRET_KEY'] = 'JOLASSQ321'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.jinja_env.add_extension('jinja2.ext.loopcontrols')

login_manager = LoginManager()
login_manager.init_app(app)

devicedatas = session.query(vega).all()
session.commit()

if len(devicedatas) != 0:
    # print("Creating Dash Application")
    create_dash_application(app)
    create_dash_application2(app)

config = configparser.ConfigParser()
deviceconfig = configparser.ConfigParser()
cdrconfig = configparser.ConfigParser()

api_key = ""
proxy_url = ""
# below converts json to dictionary from jinja template

def basename(text):
    final_dictionary = json.loads(text)
    # print(final_dictionary['lastupdate'])
    return final_dictionary


app.add_template_filter(basename)
app.jinja_env.filters['basename'] = basename

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument("window-size=1200x600")
# chrome_options.add_argument("--start-maximized")
# ser = Service(os.getcwd() + "\\chromedriver.exe")
chrome_driver =[]

subcdr_menu = []
config.read(os.path.abspath('./config.ini'))
api_key = config['API']['KEY']
proxy_url = config['PROXY']['URL']
sysport = int(config['SYSTEM']['PORT'])
flaskdebug = int(config['SYSTEM']['DEBUG'])


snmpagentenable = False
try:
    snmpagentenable = bool(config["SNMPAGENT"]["ENABLE"])
except Exception as e:
    print (e)
    snmpagentenable = False
    print("SNMPAGENT not configured or incorrect config. Setting it to OFF")



# &*^R_______-------Driver has to be initialized separately for every use, even u keep below here {ERROR} %^$%^$%^____

# driver = webdriver.Chrome(service=ser, options=chrome_options)
# driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)

# flickeT_HOMESCREEN
@app.before_request
def before_request():
    myurlhost = 'https://' + request.host

    config.read(os.path.abspath('./config.ini'))
    deviceconfig.read(os.path.abspath('./deviceconfig.ini'))
    cdrconfig.read(os.path.abspath('./cdrconfig.ini'))

    api_key = config['API']['KEY']
    proxy_url = config['PROXY']['URL']
    chrome_driver.append(os.getcwd() + config['DRIVERPATH']['CHROMEPATH'])
    if not subcdr_menu:
        for keys in list(cdrconfig.sections()):
            print("mySUbmenu's:", keys)
            subcdr_menu.append(keys)


    set_flicket_config()
    if request.endpoint != 'login' and request.endpoint != 'logout' and request.endpoint != 'dashboard' and request.endpoint != 'start' and '/static/' not in request.path and '/dash/' not in request.path:
        try:
            myuser = FlicketUser.query.filter_by(username=current_user.fullname + current_user.mobile[5:10]).first()

            if not myuser:
                print("default admin login")
                myuser = FlicketUser.query.filter_by(username="admin").first()

            myuser.get_token()
            db.session.commit()

            # print(myuser)
            g.user = myuser

            # reset the user token if the user is authenticated and token is expired.
            if g.user.is_authenticated and hasattr(g.user, 'token') and not g.user.disabled:

                if FlicketUser.check_token(g.user.token) is None:
                    g.user.get_token()
                    db2.session.commit()

            # used in the page footer
            g.__version__ = __version__

            # page title
            try:
                application_title = FlicketConfig.query.first().application_title
                db2.session.commit()
                print("level4")
                g.application_title = application_title
            except:
                db2.session.rollback()
                g.application_title = "Flicket"
        except:
            pass

@app.route('/quicksnapshot')
@login_required
def quicksnapshot():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        print('notsecure')
        myurlhost = ''
    print(myurlhost)
    resp = flask.make_response(render_template('uptimek.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, active10='active',uptimekurl=config["SYSTEM"]["UPTIMEKURL"]))
    resp.headers['X-Frame-Options']='allowall'
    return resp


@app.route('/performancegraph')
@login_required
def test():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        print('notsecure')
        myurlhost = ''
    print(myurlhost)
    return render_template('test.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, active10='active')

@app.route('/alllogs')
@login_required
def alllogs():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        logs = session.query(Auditlogs).order_by(Auditlogs.ts.desc()).limit(50)
        return render_template("alllogs.html",active_var65='active',
                               alllogs=logs, subcdr_menu=subcdr_menu, myurlhost=myurlhost)
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))

# *&(**& _______ SNMP ALARMS   __________________

@app.route('/snmp/viewalarm')
@login_required
def viewalarms():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    Alarm = session.query(snmpAlerts).all()
    return render_template("snmp/viewalarms.html", show60="show", active_var60='active', active_var63='active',
                           Alarms=Alarm, subcdr_menu=subcdr_menu, myurlhost=myurlhost)

@app.route('/snmp/viewtriggerdetail/<trigid>')
@login_required
def viewtriggerdetail(trigid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        result2 = session.query(snmpRules).filter(snmpRules.id == trigid).first()
        session.commit()
        return render_template("snmp/edittrigger.html",show60="show",active_var60='active',active_var62='active',data=result2, subcdr_menu=subcdr_menu, myurlhost=myurlhost)
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


@app.route('/snmp/viewtriggers')
@login_required
def viewtriggers():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    Trigger = session.query(snmpRules).all()
    return render_template("snmp/viewtriggers.html", show60="show", active_var60='active', active_var62='active',
                           Triggers=Trigger, subcdr_menu=subcdr_menu, myurlhost=myurlhost)


@app.route('/snmp/newtriggers')
@login_required
def newtriggers():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    return render_template("snmp/newtrigger.html", show60="show", active_var60='active', active_var61='active',
                            subcdr_menu=subcdr_menu, myurlhost=myurlhost)


@app.route('/snmp/newtriggerpost', methods=['GET', 'POST'])
@login_required
def newtriggerposts():
    print("alpha")
    oid = request.form["oid"]
    name = request.form["name"]
    email = request.form["email"]
    semail = request.form["semail"]
    ssnmp = request.form["ssnmp"]
    snotification = request.form["snotification"]

    update = request.form["update"]
    print(oid,update)
    if update == 'False':
        if oid is None :
            return redirect('/fwu/newjob/Error.All+params+required')
        else:
            Trigger = snmpRules(oid=oid, name=name, emailrecipients=email,sendemail=semail,forwardonsnmp=ssnmp,sendwebnotification=snotification)
            session.add(Trigger)
            session.commit()

            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename='Trigger',action="A Trigger is created with OID: {} by {}.".format(oid,current_user.fullname))
            session.add(ins2)
            session.commit()

            flash("Created sucessfully", 'success')
            return redirect('/snmp/viewtriggers')
    elif update == 'True':
        id = request.form["id"]
        session.query(snmpRules).filter_by(id=id).update(
            dict(oid=oid, name=name, emailrecipients=email,sendemail=semail,forwardonsnmp=ssnmp,sendwebnotification=snotification))
        session.commit()
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='Trigger',
                         action="A Trigger is updated with OID: {} by {}.".format(
                             oid, current_user.fullname))
        session.add(ins2)
        session.commit()
        flash("Updated sucessfully",'success')
        return redirect('/snmp/viewtriggers')


@app.route('/snmp/triggerdelete/<trigid>')
@login_required
def triggerdelete(trigid):
    if current_user.isadmin == "true":
        trigdetails = session.query(snmpRules).filter(snmpRules.id == trigid).first()
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='Trigger',
                         action="A Trigger is removed with OID: {}  by {}.".format(
                             trigdetails.oid, current_user.fullname))
        session.add(ins2)
        session.commit()

        session.query(snmpRules).filter(snmpRules.id == trigid).delete()
        session.commit()
        flash('Trigger Deleted','Success')
        return redirect('/snmp/viewtriggers')
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


# &*(((*#&%  ____ Auto Provisioning  ______
@app.route('/autoprovisiondef')
@login_required
def autoprovisiondef():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    with open(os.getcwd() + '/provisioning/defaultscript.txt') as f:
        contents = f.read()
        print(contents)
    return render_template('textarea.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show42='show',
                           active40='active',
                           active_var40='active', contents=contents)


# macid dashboard
@app.route('/autoprovisionmacid')
@login_required
def autoprovisionmacid():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
  
    allfiles = []
    os.chdir(os.getcwd() + '/provisioning/')
    for file in glob.glob("*.txt"):
        if not file == 'defaultscript.txt':
            print('filename: ', file)
            allfiles.append(file)

    os.chdir("../")
    return render_template('autoprovmacid.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show42='show',
                           active40='active',
                           active_var41='active', allfiles=allfiles)


# for mac delete
@app.route('/autoprovmaciddelete/<filename>', methods=['GET', 'POST'])
@login_required
def autoprovmaciddelete(filename):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        os.remove(os.getcwd() + '/provisioning/' + filename)
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='AutoProvisioning',
                         action="MacId {} from autoprovisioning is removed by {}.".format(filename.split('.')[0],
                                                                                 current_user.fullname))
        session.add(ins2)
        session.commit()
        flash('Successfully Deleted MacID')
        return redirect(url_for('autoprovisionmacid'))
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


# for view/edit
@app.route('/autoprovmacid/<filename>', methods=['GET', 'POST'])
@login_required
def autoprovmacidfile(filename):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    with open(os.getcwd() + '/provisioning/' + filename) as f:
        contents = f.read()
        print(contents)
    typeis = 'macid'
    return render_template('textarea.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show42='show',
                           active40='active',
                           active_var41='active', contents=contents, typeis=typeis, filename=filename)


# for create new mac
@app.route('/createmacid/<filename>', methods=['GET', 'POST'])
@login_required
def createmacid(filename):
    if current_user.isadmin == "true":
        defaulttext = request.form['defaulttext']
        with open(os.getcwd() + '/provisioning/' + filename + '.txt', 'w') as f:
            f.writelines(defaulttext)

        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='AutoProvisioning',action="New MacId created with name {} in AutoProvisioning by {}.".format(filename,current_user.fullname))
        session.add(ins2)
        session.commit()
        return defaulttext
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))



@app.route('/templatetext')
@login_required
def templatetext():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    with open(os.getcwd() + '/provisioning/template.ini') as f:
        contents = f.read()
        print(contents)
    return contents


@app.route('/createmacpage')
@login_required
def createmacpage():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        return render_template('createmacid.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show42='show',
                               active40='active',
                               active_var41='active')
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


@app.route('/writedefaulttext', methods=['GET', 'POST'])
@login_required
def writedefaulttext():
    defaulttext = request.form['defaulttext']
    filename = request.form['filename']
    with open(os.getcwd() + '/provisioning/' + filename, 'w') as f:
        f.writelines(defaulttext)

    if filename == 'defaultscript.txt':
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='AutoProvisioning', action="AutoProvisioning Default text is Updated by {}.".format(current_user.fullname))
        session.add(ins2)
        session.commit()
    else:
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='AutoProvisioning',
                         action="MacId {} textdetails are Updated by {}.".format(filename.split('.')[0],current_user.fullname))
        session.add(ins2)
        session.commit()
    return defaulttext


#  &^$#%^((& ____ DIAGNOSTICS ________ *&^%$#
@app.route('/diagnostics/ping/')
@login_required
def diag_ping():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    return render_template('diag_ping.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show4='show',
                           active_var3='active',
                           active_var32='active')


# print pingcherck code
import subprocess

@app.route('/pingcheck', methods=['GET', 'POST'])
@login_required
def ping_check():
    
    url = request.form['ping']
    # allpings = ping(url, verbose=True)
    allpings = subprocess.check_output("ping -c 1 " + url, shell=True)
    # response_ping = []
    # for pings in allpings:
    #     response_ping.append(pings)
    # print(response_ping)
    # return str(response_ping)[1:][:-1]
    return str(allpings)


@app.route('/diagnostics/mosgraph/')
@login_required
def diag_mosgraph():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    return render_template('diag_mosgraph.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show4='show',
                           active_var3='active',
                           active_var31='active')


@app.route('/diagnostics/speedtest/')
@login_required
def diag_speedtest():
    
    servers = []
    # If you want to test against a specific server
    # servers = [1234]
    threads = None
    # If you want to use a single threaded test
    # threads = 1
    s = speedtest.Speedtest()
    s.get_servers(servers)
    s.get_best_server()
    s.download(threads=threads)
    s.upload(threads=threads)
    s.results.share()
    results_dict = s.results.dict()
    print(results_dict)
    print('megabits: ', Mb(bits=results_dict['upload']))
    results_dict['upload'] = str(Mb(bits=results_dict['upload']))[0:7]
    results_dict['download'] = str(Mb(bits=results_dict['download']))[0:7]

    json_object = json.dumps(results_dict, indent=4)
    return json_object


@app.route('/diagnostics/nodes')
@login_required
def diag_nodes():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    result2 = session.query(monitoringNodes).all()
    return render_template('monitoringnodes.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=result2,
                           show4='show',
                           active_var3='active', active_var30='active')


@app.route('/diagnostics/newnode')
@login_required
def diag_new_node():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        return render_template('newmonitoringnode.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show4='show',
                               active_var3='active',
                               active_var30='active')
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


@app.route('/diagnostics/newnoderegister', methods=['POST'])
@login_required
def diag_new_node_post():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        newnode = monitoringNodes(name=request.form['name'], ip=request.form['ip'], description=request.form['description'])
        session.add(newnode)
        session.commit()
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename=request.form['name'],ip=request.form['ip'],
                         action="A Node {} under diagnostics is added with ip: {} by {}.".format(request.form['name'], request.form['ip'],
                                                                                      current_user.fullname))
        session.add(ins2)
        session.commit()

        return redirect('/diagnostics/nodes')
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


# %$^&^* MOS CALCULATOR &*$%#%^&*(
@app.route('/diagnostics/calculatemos/<mid>', methods=['GET', 'POST'])
@login_required
def moscal(mid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    result2 = session.query(monitoringNodes).filter_by(id=mid).first()
    print(result2.ip)
    if 'http' in result2.ip:
        mosscore = "invalid"
    else:
        try:
            mosscore = getmosscore(result2.ip)
            mosscore = str(mosscore)[0:8]
            print("mosscore for " + result2.ip + " is " + str(mosscore))
            newscore = mosScores(ip=result2.ip, score=mosscore)
            session.add(newscore)
            session.commit()
        except:
            mosscore = 'None'
    return mosscore


@app.route('/diagnostics/deletenode/<cid>', methods=['GET', 'POST'])
@login_required
def deletenode(cid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        node= session.query(monitoringNodes).filter(monitoringNodes.id == cid).first()
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename=node.name, ip=node.ip,
                         action="A Node {} under diagnostics is removed by {}.".format(node.name,current_user.fullname))
        session.add(ins2)
        session.commit()

        session.query(monitoringNodes).filter(monitoringNodes.id == cid).delete()
        session.commit()
        flash("Device Deleted Successfully")
        return redirect(url_for("diag_nodes"))
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


@app.route('/diagnostics/mos')
@login_required
def diag_mos():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    result2 = session.query(mosScores).all()
    return render_template('mosreports.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=result2, show4='show',
                           active_var3='active',
                           active_var31='active')


#  *&^$#$%#%^&$____------ BACKUP LICENSE'S   ________________----------

@app.route('/licenseview')
@login_required
def licenseview():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    backups = session.query(DeviceLicenses).all()
    devicenames = session.query(deviceinfo.device_name).distinct()
    return render_template('license_info.html', devicenames=devicenames, data=backups, subcdr_menu=subcdr_menu,
                           myurlhost=myurlhost,
                           active16='active')


@app.route('/backuplicensecreate', methods=['GET', 'POST'])
@login_required
def backuplicensecreate():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    allkeys = []
    device_name = request.form['device_name']
    description = request.form['description']
    devicedata = session.query(deviceinfo).filter_by(device_name=device_name).first()
    for key, value in deviceconfig.items(devicedata.device_type):
        allkeys.append(key)

    backuplicensefilepath = ""
    if "backuplicensefilepath" in allkeys:
        backuplicensefilepath = deviceconfig[devicedata.device_type]['backuplicensefilepath']

    # chrome_options = Options()
    # chrome_options.add_argument("--headless")
    # chrome_options.add_argument('window-size=1920x1080')
    # # ser = Service(os.getcwd() + "\\chromedriver.exe")
    # chrome_driver = os.getcwd() + "\\chromedriver.exe"

    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(path, backuplicensefilepath)
    path = os.path.join(path, 'temp/')
    isExist = os.path.exists(path)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(path)
    prefs = {"download.default_directory": path}

    filename = devicedata.device_type + '_' + device_name + '_' + datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    print(path, filename)
    for f in os.listdir(path):
        os.remove(os.path.join(path, f))

    chrome_options.add_experimental_option("prefs", prefs)

    # driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
    try:
        if "backuplicense" in allkeys:
            driver.get(devicedata.ip)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(Keys.RETURN)
            print("done")

            dconfigparams = deviceconfig[devicedata.device_type]['backuplicense'].split(',')
            url = devicedata.ip + dconfigparams[0]
            print(url)
            driver.get(url)
            time.sleep(int(deviceconfig[devicedata.device_type]["delay"]))
            driver.get_screenshot_as_file("capture{}.png".format('newfirst'))
            tabledata = []
            for x in range(1, len(dconfigparams)):
                try:
                    element = driver.find_element(By.XPATH, dconfigparams[x])
                    elementtype = element.tag_name
                    print("type of element is ", elementtype)
                    if elementtype != 'input' and elementtype != 'a':
                        tabledata.append(element.text if element.text else "none")
                    else:
                        element.click()
                        time.sleep(7)
                        driver.get_screenshot_as_file("capture{}.png".format(x))
                except:
                    print("exception")
                    flash("wrong xpath in configs", 'error')
                    driver.get_screenshot_as_file("capture{}.png".format(x))

            allfiles = os.listdir(path)
            print(allfiles)
            extensiontype = allfiles[0].split('.')
            src = os.path.realpath(os.path.join(path, allfiles[0]))
            os.rename(src, os.path.join(path, '..', filename + "." + extensiontype[len(extensiontype) - 1]))
            for f in os.listdir(path):
                os.remove(os.path.join(path, f))
            base_path = os.path.dirname(os.path.dirname(path))
            ins = DeviceLicenses(name=tabledata[0], email=tabledata[1], product=tabledata[2], maxcalls=tabledata[3],
                                 description=description, deviceip=devicedata.ip, device_type=devicedata.device_type,
                                 device_name=device_name, backupfilepath=os.path.join(base_path,
                                                                                      filename + "." + extensiontype[
                                                                                          len(extensiontype) - 1]))
            session.add(ins)
            session.commit()
            flash(" Backup Created as {}".format(filename + "." + extensiontype[len(extensiontype) - 1]), 'success')
            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename=device_name, ip=devicedata.ip,
                             action="A license backup {} has been created for device {} by {}.".format(
                                 filename + "." + extensiontype[len(extensiontype) - 1], device_name, current_user.fullname))
            session.add(ins2)
            session.commit()
        else:
            flash("no backup, xpath defined in deviceconfig.ini", 'error')

    except:
        flash("failed to create a backup", 'error')
    driver.implicitly_wait(1)
    driver.quit()
    return redirect(url_for("licenseview"))


@app.route('/backuplicenserestore/<bid>', methods=['GET', 'POST'])
@login_required
def backuplicenserestore(bid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''


    allkeys = []
    backups = session.query(DeviceLicenses).filter_by(id=bid).first()
    print(backups)
    filename = backups.backupfilepath.split("\\")
    devicedata = session.query(deviceinfo).filter_by(device_name=backups.device_name).first()
    for key, value in deviceconfig.items(backups.device_type):
        allkeys.append(key)

    # driver = webdriver.Chrome(service=ser, options=chrome_options)

    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
    try:
        if "restorelicense" in allkeys:

            driver.get(devicedata.ip)   #changing from backups.deviceip
            driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['password_id']).send_keys(Keys.RETURN)
            print("Logging done")
            driver.get_screenshot_as_file("capture0.png")
            dconfigparams = deviceconfig[backups.device_type]['restorelicense'].split(',')
            url = devicedata.ip+dconfigparams[0]  #devicedata
            print(url)
            driver.get(url)
            driver.get_screenshot_as_file("capture01.png")
            time.sleep(int(deviceconfig[backups.device_type]["delay"]))
            print("loopin config xpath")
            for x in range(1, len(dconfigparams)):
                print(dconfigparams[x])
                try:
                    element = driver.find_element(By.XPATH, dconfigparams[x])
                    element.click()
                    time.sleep(1)
                    driver.get_screenshot_as_file("capture1{}.png".format(x))
                except:
                    try:
                        elm = driver.find_element(By.XPATH, dconfigparams[x])
                        print(backups.backupfilepath)
                        elm.send_keys(backups.backupfilepath)
                        driver.get_screenshot_as_file("capture2{}.png".format(x))
                    except:
                        # doesnt matter
                        pass
            flash("license Restored Successfully.", 'success')
            time.sleep(1)
            driver.get_screenshot_as_file("captureend.png")
            # Activity LOG AUDIT

            ins2 = Auditlogs(devicename=backups.device_name, ip=backups.deviceip,
                             action="A license backup {} has been restored for device {} by {}.".format(
                                 filename[len(filename)-1], backups.device_name, current_user.fullname))
            session.add(ins2)
            session.commit()
    except:
        print("not working")
        flash("failed to restore", 'error')
        pass

    driver.implicitly_wait(1)
    driver.quit()
    return redirect(url_for("licenseview"))


#  *&^$#$%#%^&$____------ BACKUP LICENSE'S   ________________ END  END  ----------

#  *&^$#$%#%^&$____------ BACKUP AND RESTORE ________________----------
@app.route('/BaRview')
@login_required
def backupandrestore():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    backups = session.query(Devicebackups).all()
    if current_user.region == "None" or current_user.region is None:
        devicenames = session.query(deviceinfo).distinct()
        session.commit()
    else:
        devicenames = session.query(deviceinfo).filter(deviceinfo.region == current_user.region).distinct()
        session.commit()
    # devicenames = session.query(deviceinfo).distinct()
    return render_template('backup_and_restore.html', devicenames=devicenames, data=backups, subcdr_menu=subcdr_menu,
                           myurlhost=myurlhost,
                           active15='active')


@app.route('/restoreother/<dtype>', methods=['GET', 'POST'])
@login_required
def restoreother(dtype):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    alldata = {}
    devices = session.query(deviceinfo).filter_by(device_type=dtype).all()
    for index, data in enumerate(devices):
        alldata[index] = {'device_name': data.device_name, 'device_type': data.device_type, 'ip': data.ip}
    return alldata


# other machine backup
@app.route('/backuprestoreother/<bid>/<dname>', methods=['GET', 'POST'])
@login_required
def otherbackuprestore(bid, dname):
    allkeys = []
    backups = session.query(Devicebackups).filter_by(id=bid).first()
    devicedata = session.query(deviceinfo).filter_by(device_name=dname).first()
    for key, value in deviceconfig.items(backups.device_type):
        allkeys.append(key)
    # driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
    try:
        sendsnmpagentmessage("noderestoration")
        if "restoreother" in allkeys:
            driver.get(devicedata.ip)
            driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['password_id']).send_keys(Keys.RETURN)
            print("Logging done")
            driver.get_screenshot_as_file("captureFIRST.png")

            dconfigparams = deviceconfig[backups.device_type]['restoreother'].split(',')
            url = devicedata.ip + dconfigparams[0]
            driver.get(url)
            time.sleep(int(deviceconfig[backups.device_type]["delay"]))
            print("loopin config xpath")

            for x in range(1, len(dconfigparams)):
                print('config:', dconfigparams[x])
                try:
                    # specific for sbc restore
                    if dconfigparams[x] == 'static':
                        print("static")
                        driver.implicitly_wait(8)
                        filerestore = backups.filename.lower()
                        filerestore = filerestore.replace("_", "-")
                        filerestore = filerestore.replace(".", "_") + '.tgz'
                        print(filerestore)
                        new_xpath = "//*[@id='application/archive/" + filerestore + "']//input[@value='Restore']"
                        print(new_xpath)
                        element = driver.find_element(By.XPATH, new_xpath)
                        driver.execute_script("arguments[0].click();", element)
                        driver.get_screenshot_as_file("capture1{}.png".format(x))
                        print("done static part")
                        time.sleep(2)
                    else:
                        print("fromelse-exception")
                        raise Exception("goto except clause")
                        pass
                except:
                    print("something failed")
                    try:
                        print("trying clicks")
                        element = driver.find_element(By.XPATH, dconfigparams[x])
                        # driver.execute_script("arguments[0].click();", element)
                        element.click()
                        print(x)
                        if 3 <= x < 4:
                            print("bigsleep")
                            time.sleep(20)
                        else:
                            print("small-nap")
                            time.sleep(3)
                        driver.get_screenshot_as_file("capture1{}.png".format(x))
                    except:
                        try:
                            print("trying filenamesend")
                            time.sleep(2)
                            elm = driver.find_element(By.XPATH, dconfigparams[x])
                            elm.send_keys(backups.backupfilepath)
                            driver.get_screenshot_as_file("capture2{}.png".format(x))
                        except:
                            print('failed')
                            flash("Restoration Failed.", 'error')
                            pass
            flash("Config Restored Successfully.", 'success')
            time.sleep(3)
            driver.get_screenshot_as_file("captureend.png")

            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename=devicedata.device_name, ip=devicedata.ip,
                             action="A config backup of device {} has been restored to device {} by {}.".format(
                                 backups.device_name, devicedata.device_name, current_user.fullname))
            session.add(ins2)
            session.commit()
        else:

            flash('Restoreother field does not exist in deviceconfig of ' + backups.device_type, 'error')
    except:
        print("not working")
        flash("failed to restore", 'error')
        pass

    driver.implicitly_wait(4)
    driver.quit()
    return redirect(url_for("backupandrestore"))


# generic current machine backup
@app.route('/backuprestore/<bid>', methods=['GET', 'POST'])
@login_required
def backuprestore(bid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''

    allkeys = []
    backups = session.query(Devicebackups).filter_by(id=bid).first()
    print(backups)
    devicedata = session.query(deviceinfo).filter_by(device_name=backups.device_name).first()
    for key, value in deviceconfig.items(backups.device_type):
        allkeys.append(key)

    # driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
    try:
        if "restore" in allkeys:
            driver.get(devicedata.ip) #changing backups.deviceip to devicedata.ip
            driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[backups.device_type]['password_id']).send_keys(Keys.RETURN)
            print("Logging done")
            driver.get_screenshot_as_file("captureFIRST.png")

            dconfigparams = deviceconfig[backups.device_type]['restore'].split(',')
            url = devicedata.ip + dconfigparams[0] #changing backups.deviceip to devicedata.ip
            driver.get(url)
            time.sleep(int(deviceconfig[backups.device_type]["delay"]))
            driver.get_screenshot_as_file("captureSecond.png")
            print("loopin config xpath")

            for x in range(1, len(dconfigparams)):
                print('config : ', dconfigparams[x])
                try:
                    # specific for sbc restore
                    if dconfigparams[x] == 'static':
                        print('inside static')
                        new_xpath = '//*[@id="application/archive/' + backups.filename + '"]//input[@value="Restore"]'
                        element = driver.find_element(By.XPATH, new_xpath)
                        element.click()
                        time.sleep(2)
                        print("static complete")
                        driver.get_screenshot_as_file("capture1{}.png".format(x))
                    else:
                        raise Exception("goto except clause")
                        pass
                except:
                    print('something failed')
                    try:
                        element = driver.find_element(By.XPATH, dconfigparams[x])
                        element.click()
                        time.sleep(6)
                        driver.get_screenshot_as_file("capture1{}.png".format(x))
                    except:
                        try:
                            elm = driver.find_element(By.XPATH, dconfigparams[x])
                            print(backups.backupfilepath)
                            elm.send_keys(backups.backupfilepath)
                            driver.get_screenshot_as_file("capture2{}.png".format(x))
                        except:
                            print('all failed')
                            raise Exception("goto except clause")
                            # doesnt matter
                            pass
            flash("Config Restored Successfully.", 'success')
            time.sleep(3)
            driver.get_screenshot_as_file("captureend.png")

            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename=backups.device_name, ip=backups.deviceip,
                             action="A config backup {} has been restored for device {} by {}.".format(
                                 backups.filename, backups.device_name, current_user.fullname))
            session.add(ins2)
            session.commit()

    except:
        print("not working")
        flash("failed to restore, Backup Doesnt Exist In Machine.", 'error')
        pass

    driver.implicitly_wait(1)
    driver.quit()
    return redirect(url_for("backupandrestore"))


@app.route('/backupcreate', methods=['GET', 'POST'])
@login_required
def backupcreate():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    allkeys = []
    device_name = request.form['device_name']
    description = request.form['description']
    devicedata = session.query(deviceinfo).filter_by(device_name=device_name).first()
    for key, value in deviceconfig.items(devicedata.device_type):
        allkeys.append(key)

    backupfilepath = ""
    if "backupfilepath" in allkeys:
        backupfilepath = deviceconfig[devicedata.device_type]['backupfilepath']

    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument("window-size=1200x600")
    # ser = Service(os.getcwd() + "\\chromedriver.exe")
    # chrome_options = Options()
    # chrome_options.add_argument("--headless")
    # chrome_options.add_argument('window-size=1920x1080')
    # # ser = Service(os.getcwd() + "\\chromedriver.exe")
    # chrome_driver = os.getcwd() + "\\chromedriver.exe"

    path = os.path.dirname(os.path.abspath(__file__))
    path = os.path.join(path, backupfilepath)
    path = os.path.join(path, 'temp/')
    isExist = os.path.exists(path)
    if not isExist:
        # Create a new directory because it does not exist
        os.makedirs(path)
    prefs = {"download.default_directory": path}

    filename = devicedata.device_type + '_' + device_name + '_' + datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    print(path, filename)

    chrome_options.add_experimental_option("prefs", prefs)

    # driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
    try:
        if "backup" in allkeys:
            driver.get(devicedata.ip)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(Keys.RETURN)
            print("done")
            dconfigparams = deviceconfig[devicedata.device_type]['backup'].split(',')
            url = devicedata.ip + dconfigparams[0]
            driver.get(url)
            time.sleep(int(deviceconfig[devicedata.device_type]["delay"]))

            for x in range(1, len(dconfigparams)):
                try:
                    # for vega devices in mind
                    element = driver.find_element(By.XPATH, dconfigparams[x])
                    elementtype = element.tag_name
                    print("type of element is ", elementtype)
                    element.click()
                    time.sleep(6)
                except:
                    # *********** FOR SBC in mind ***********________********&^^^^^%$#@@
                    select = Select(driver.find_element_by_name('backup_type'))
                    select.select_by_value('configuration')
                    driver.find_element(By.XPATH, '//*[@id="name"]').send_keys(filename)
                    driver.find_element(By.XPATH, '//*[@id="name"]').send_keys(Keys.RETURN)
                    time.sleep(4)
                    element = driver.find_element(By.XPATH,
                                                  '//*[@id = "notif_warn"]/table/tbody/tr/td[3]/form[1]/input[1]')
                    element.click()
                    time.sleep(7)
                    driver.get_screenshot_as_file("capture{}.png".format(x))

            allfiles = os.listdir(path)
            extensiontype = allfiles[0].split('.')
            print(filename + "." + extensiontype[1])
            backup_filename = filename + "." + extensiontype[1]
            src = os.path.realpath(os.path.join(path, allfiles[0]))
            os.rename(src, os.path.join(path, '..', filename + "." + extensiontype[1]))
            for f in os.listdir(path):
                os.remove(os.path.join(path, f))
            base_path = os.path.dirname(os.path.dirname(path))
            ins = Devicebackups(filename=backup_filename, description=description, deviceip=devicedata.ip,
                                device_type=devicedata.device_type,
                                device_name=device_name,
                                backupfilepath=os.path.join(base_path, filename + "." + extensiontype[1]))
            session.add(ins)
            session.commit()

            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename=device_name,ip=devicedata.ip,action="A Config backup {} has been created for device {} by {}.".format(filename + "." + extensiontype[1],device_name,current_user.fullname))
            session.add(ins2)
            session.commit()
            flash(" Backup Created as {}".format(filename + "." + extensiontype[1]), 'success')
        else:
            flash("no backup, xpath defined in deviceconfig.ini", 'error')

    except Exception as eas:
        print(eas)
        flash("failed to create a backup", 'error')
    driver.implicitly_wait(1)
    driver.quit()
    return redirect(url_for("backupandrestore"))


#  *&^$#$%#%^&$____------ BACKUP AND RESTORE ________________ END END----------

#  *&^$#$%#%^&$____------ QUICK CONFIG   ________________----------
@app.route('/configupdate', methods=['GET', 'POST'])
@login_required
def configupdate():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        allkeys = []
        formdata = {}
        dtype = request.form['dtype']
        dname = request.form['dname']
        for key, value in deviceconfig.items(dtype):
            allkeys.append(key)

        alldata = {}
        devicedata = session.query(deviceinfo).filter(deviceinfo.device_name == dname).first()
        print(devicedata.ip)

        # driver = webdriver.Chrome(service=ser, options=chrome_options)
        driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
        try:
            driver.get(devicedata.ip)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(Keys.RETURN)
            print("done")
            if "quickconfigparams" in allkeys:
                dconfigparams = deviceconfig[dtype]['quickconfigparams'].split(',')
                print(dconfigparams)
                for param in dconfigparams:
                    formdata[param] = request.form[param]
                    print(formdata[param])
                    readparam = deviceconfig[dtype][param].split(',')
                    print(readparam)
                    url = devicedata.ip + readparam[0]
                    driver.get(url)
                    time.sleep(int(deviceconfig[devicedata.device_type]["delay"]))
                    try:
                        xpelement = driver.find_element(By.XPATH, readparam[1])
                        xpelement.send_keys(Keys.CONTROL, 'a')
                        xpelement.send_keys(Keys.BACKSPACE)
                        xpelement.send_keys(formdata[param])
                        alldata[param] = xpelement.get_attribute("value")
                        driver.get_screenshot_as_file("capture0.png")
                        xpelement.send_keys(Keys.RETURN)

                        driver.get_screenshot_as_file("capture01.png")
                        time.sleep(int(deviceconfig[devicedata.device_type]["delay"]))
                        try:
                            driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                            driver.find_element_by_name('save').click()
                            driver.get_screenshot_as_file("capture03.png")
                        except:
                            print("exception while submiting")
                            pass
                        try:
                            driver.switch_to.alert.accept()
                        except:
                            pass
                        driver.get_screenshot_as_file("capture.png")
                    except:
                        print("exception occured")
                        flash("no such elements found")
                        pass
            else:
                flash("No quickconfigparams in deviceconfig")
        except:
            flash("failed to fetch details, check deviceconfig.ini for {}".format(dtype))
            pass
        driver.implicitly_wait(1)
        driver.quit()
        # return redirect(url_for('deviceconfigs',dname=dname,dtype=dtype, **request.args))
        return render_template("deviceconfig.html", alldata=alldata, dconfigparams=dconfigparams, dtype=dtype,
                               dname=dname)
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


@app.route('/deviceconfig/<dname>/<dtype>', methods=['GET', 'POST'])
@login_required
def deviceconfigs(dname, dtype):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    allkeys = []
    for key, value in deviceconfig.items(dtype):
        allkeys.append(key)
    alldata = {}
    devicedata = session.query(deviceinfo).filter(deviceinfo.device_name == dname).first()
    print(devicedata.ip)
    # chrome_options = Options()
    # chrome_options.add_argument("--headless")
    # chrome_options.add_argument("window-size=1200x600")
    # # ser = Service(os.getcwd() + "\\chromedriver.exe")
    # chrome_driver = os.getcwd() + "\\chromedriver.exe"

    # driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])

    try:
        if "quickconfigparams" in allkeys:
            driver.get(devicedata.ip)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['username_id']).send_keys(devicedata.username)
            form = driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(devicedata.password)
            driver.find_element(
                By.XPATH, deviceconfig[devicedata.device_type]['password_id']).send_keys(Keys.RETURN)
            print("done")

            dconfigparams = deviceconfig[dtype]['quickconfigparams'].split(',')
            print(dconfigparams)
            for param in dconfigparams:
                readparam = deviceconfig[dtype][param].split(',')
                print(readparam)
                url = devicedata.ip + readparam[0]
                driver.get(url)
                time.sleep(int(deviceconfig[devicedata.device_type]["delay"]))
                try:
                    xpelement = driver.find_element(By.XPATH, readparam[1])
                    print(xpelement.get_attribute("value"))
                    alldata[param] = xpelement.get_attribute("value")
                except:
                    flash("no such elements found")
                    pass
                # driver.get_screenshot_as_file("capture.png")
                print(alldata)
            driver.implicitly_wait(1)
            driver.quit()
            return render_template("deviceconfig.html", alldata=alldata, subcdr_menu=subcdr_menu, dconfigparams=dconfigparams, dtype=dtype,
                                   dname=dname)
        else:
            flash("No quickconfigparams in deviceconfig, check deviceconfig.ini for {}".format(dtype))
    except:
        print("failed")
        flash("failed to fetch details, check deviceconfig.ini for {}".format(dtype))
        pass
    driver.implicitly_wait(1)
    driver.quit()
    return render_template("deviceconfig.html")


#  *&^$#$%#%^&$____------ QUICK CONFIG   ________________ END END ----------
#  *&^$#$%#%^&$____------ DASHBOARD   ________________----------
@app.route('/dashboard')
@login_required
def dashboard():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        # creates and fetch new entry everytime
        # exec(open("./chromeheadless.py").read())
        device_dict = {}
        device_params_dict = {}
        devicecontrol_params_dict = {}
        sshdevicecontrol_params_dict = {}
        device_types_list = []
        temp2 = session.query(vega.device_type).distinct()
        session.commit()
        for data in temp2:
            print("data is : ",data[0])
            device_types_list.append(data[0])
            devicealldata = []
            apple = session.query(deviceinfo).filter(
                deviceinfo.device_type == data.device_type).all()
            for data2 in apple:
                # print(data2.device_name)
                if not session.query(vega).filter(vega.device_name == data2.device_name).order_by(
                    vega.Time_created.desc()).first() is None:

                    devicealldata.append(session.query(vega).filter(vega.device_name == data2.device_name).order_by(
                        vega.Time_created.desc()).first())
            # print(devicealldata)
            device_dict[data[0]] = devicealldata
            for device in device_types_list:
                deviceparams = []
                devicecontrol_params = []
                devicecontrol_params.append(
                    deviceconfig[device]['controlparams'].split(','))
                sshdevicecontrol_params = []
                if deviceconfig.has_option(device,'sshcontrolparams'):
                    sshdevicecontrol_params.append(
                        deviceconfig[device]['sshcontrolparams'].split(','))    
                # print(deviceconfig[device]['controlparams'].split(','))
                deviceparams.append(deviceconfig[device]['readparams'].split(','))
                if deviceconfig.has_option(device,'sshreadparams'):
                    deviceparams[0].extend(deviceconfig[device]['sshreadparams'].split(','))
                print(deviceparams)
                device_params_dict[data[0]] = deviceparams

                devicecontrol_params_dict[data[0]] = devicecontrol_params
                sshdevicecontrol_params_dict[data[0]] = sshdevicecontrol_params



        # *** IMPORTANT VARIABLES THAT BEING SENT TO FRONT END *****
        # print(devicecontrol_params_dict)
        # print(device_params_dict ,"\n")
        # print(device_dict['sangoma acid'][1].paramname)
        # print(device_dict)
        # print(device_types_list)

        result2 = session.query(vega).order_by(vega.id.desc()).limit(300).all()
        session.commit()
        allkeys = vega.__table__.columns.keys()
        return render_template('dashboard.html', myurlhost=myurlhost, subcdr_menu=subcdr_menu, allkeys=allkeys,active='active',data=device_dict,devicecontrol_params_dict=devicecontrol_params_dict,device_params_dict=device_params_dict,device_types_list=device_types_list, alldata=result2, sshdevicecontrol_params_dict=sshdevicecontrol_params_dict, currdate=datetime.now())
    else:

        device_dict = {}
        device_params_dict = {}
        devicecontrol_params_dict = {}
        device_types_list = []
        temp2 = session.query(vega.device_type).distinct()
        session.commit()
        for data in temp2:
            device_types_list.append(data[0])
            devicealldata = []
            apple = session.query(deviceinfo).filter(
                deviceinfo.device_type == data.device_type).filter(deviceinfo.region == current_user.region).all()
            for data2 in apple:
                # print(data2.device_name)
                if not session.query(vega).filter(vega.device_name == data2.device_name).order_by(
                        vega.Time_created.desc()).first() is None:
                    devicealldata.append(session.query(vega).filter(vega.device_name == data2.device_name).order_by(
                        vega.Time_created.desc()).first())
                # print(devicealldata)
            device_dict[data[0]] = devicealldata
            for device in device_types_list:
                deviceparams = []
                devicecontrol_params = []
                devicecontrol_params.append(
                    deviceconfig[device]['controlparams'].split(','))
                # print(deviceconfig[device]['controlparams'].split(','))
                deviceparams.append(deviceconfig[device]['readparams'].split(','))
                device_params_dict[data[0]] = deviceparams
                devicecontrol_params_dict[data[0]] = devicecontrol_params
        # *** IMPORTANT VARIABLES THAT BEING SENT TO FRONT END *****
        # print(devicecontrol_params_dict)
        # print(device_params_dict ,"\n")
        # print(device_dict['sangoma acid'][1].paramname)
        # print(device_dict)
        # print(device_types_list)

        result2 = session.query(vega).order_by(vega.id.desc()).limit(200).all()
        session.commit()
        allkeys = vega.__table__.columns.keys()
        return render_template('dashboard.html', myurlhost=myurlhost, subcdr_menu=subcdr_menu, allkeys=allkeys,
                               active='active',
                               data=device_dict,
                               devicecontrol_params_dict=devicecontrol_params_dict,
                               device_params_dict=device_params_dict,
                               device_types_list=device_types_list, alldata=result2, currdate=datetime.now())



# GraphView

@app.route('/graphview')
@login_required
def graphview():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "True" or current_user.isadmin == "true":
        device_types_list = []
        temp2 = session.query(vega.device_type).distinct()
        session.commit()
        device_all_data = {}
        graph_all_param = {}
        for data in temp2:
            device_types_list.append(data[0])
            device_all_data[data[0]] = session.query(vega).filter(vega.device_type == data.device_type).order_by(
                vega.id.desc()).all()
            session.commit()
            graphparam = []

            try:
                graph = deviceconfig[data[0]]['graph'].split(',')
                print(graph)
                for item in graph:
                    print(item)
                    graphparam.append(item.split('-'))
                graph_all_param[data[0]] = graphparam
            except:
                print("no graphs for {}".format(data[0]))

        print(device_all_data)
        print(graph_all_param)
        return render_template('graphview.html', myurlhost=myurlhost, subcdr_menu=subcdr_menu, active5='active',
                               graph=graph_all_param,
                               device_types_list=device_types_list, alldata=device_all_data)
    else:
        flash('You Need Admin Privileges')
        return redirect(url_for("dashboard"))


# CDR PARSER MAIN PAGE
# @app.route('/cdrparser')
# @login_required
# def cdrparser():
#     allkeys = cdrParser.__table__.columns.keys()
#     cdrdata = session.query(cdrParser).all()
#     session.commit()
#     return render_template('cdrparser.html',subcdr_menu=subcdr_menu, allkeys=allkeys, active4='active', data=cdrdata)

@app.route('/cdrparser/<submenu>', methods=['GET', 'POST'])
@login_required
def cdrparser(submenu):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    try:
        cdrtable = cdrconfig[submenu]['Tablename']
        # importing from above variable name sqlalchemy tableobject
        the_module = importlib.import_module('orm')
        # from  orm.py getting the table object as per the table name from config
        the_class = getattr(the_module, cdrtable)
        allkeys = the_class.__table__.columns.keys()
        cdrdata = session.query(the_class).order_by(the_class.timestamp.desc()).limit(1000)
        session.commit()
        return render_template('cdrparser.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show='show',
                               allkeys=allkeys,
                               active_var='active',
                               data=cdrdata)
    except:
        flash('Wrong tablename in config', 'error')
        return render_template('cdrparser.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show='show',
                               active_var='active',
                               )


@app.route('/cdrreport/<submenu>', methods=['GET', 'POST'])
@login_required
def cdrreport(submenu):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    try:
        cdrtable = cdrconfig[submenu]['Tablename']
        # importing from above variable name sqlalchemy tableobject
        the_module = importlib.import_module('orm')
        the_class = getattr(the_module, cdrtable)
        allkeys = the_class.__table__.columns.keys()
        cdrdata = session.query(the_class.device_name).distinct()
        session.commit()
        try:
            group_report = cdrconfig[submenu]['GroupReport'].split(',')
        except:
            group_report = []
        return render_template('cdrreport.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, show2='show',
                               dtype=submenu, tablename=cdrtable,
                               group_report=group_report, active_var2='active', data=cdrdata)
    except:
        flash('Wrong tablename in config', 'error')
        return redirect(url_for('dashboard'))


# here cdrreport filter

@app.route('/cdrreports', methods=['GET', 'POST'])
@login_required
def cdrreportsearch():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    global count
    from_date = request.form['from_date']
    to_date = request.form['to_date']
    device_name = request.form['device_name']
    reports = request.form['report']
    device_table = request.form['device_table']
    device_type = request.form['device_type']
    the_module = importlib.import_module('orm')  # eg. importlib.import_module('a.b.models')
    the_class = getattr(the_module, device_table)

    # print(reports, from_date, to_date, device_name)
    if not reports == 'Hourly':
        report = getattr(the_class, reports)
        cdrdata = session.query(report, func.count(report)).filter(
            the_class.timestamp.between(from_date, to_date)).filter_by(device_name=device_name).group_by(report)
        session.commit()
    else:
        cdrdata = session.query(func.hour(the_class.timestamp), func.count('*')).filter(
            the_class.timestamp.between(from_date, to_date)).filter_by(device_name=device_name).group_by(
            func.hour(the_class.timestamp))
        print(cdrdata)
        session.commit()
    count_data = []
    for i in cdrdata:
        print("count: ", i)
        count_data.append(list(i))
    # return str(reports)+": "+str(count[0])+str(count[1])
    return render_template("cdr_filter.html", subcdr_menu=subcdr_menu, myurlhost=myurlhost, show2="show",
                           device_name=device_name,
                           device_type=device_type,
                           from_date=from_date, to_date=to_date, report=reports, group_query=count_data)


# CDR PARSER Generate Records button
@app.route('/generaterecords')
@login_required
def generaterecords():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    try:
        
        file_globals = runpy.run_path("./cdrparser.py", run_name='__main__')
        print("from globals: ",file_globals.get('status'))
        print("from globals: ", file_globals.get('myvar'))
        # exec(open("./cdrparser.py").read())
        status = file_globals.get('status')
        failed_files = file_globals.get('myvar')
        if not status:
            status = " "
        if not failed_files:
            failed_files = " "
        ins = Auditlogs(action="CDR Reports Are Generated For all the Availabel Devices",devicename="All")
        session.add(ins)
        session.commit()
        if failed_files == " ":
            return str(status) + "\n" + str(failed_files), 200
        else:
            return str(status)+"\n"+ str(failed_files), 500

    except Exception as ex:
        return 'Fail'


# Button controls from dashboard and device config controlparams action


@app.route('/controlapi/<did>/<dtype>/<dname>/<param>', methods=['GET', 'POST'])
@login_required
def controlapi(did, dtype, dname, param):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    print(did, dtype, dname, param)

    result = session.query(deviceinfo).filter(deviceinfo.device_name == dname).first()
    session.commit()
    print(result.ip)

    controldata = deviceconfig[dtype][param].split(',')
    print(controldata)
    url = result.ip+controldata[0]
    # driver = webdriver.Chrome(service=ser, options=chrome_options)
    driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver[0])
    try:
        driver.get(result.ip)
        print(result.ip)
        driver.find_element_by_xpath(deviceconfig[dtype]['username_id']).send_keys(result.username)
        form = driver.find_element_by_xpath(deviceconfig[dtype]['password_id']).send_keys(result.password)
        driver.get_screenshot_as_file("000_clicklogin.png")
        driver.find_element_by_xpath(
            deviceconfig[dtype]['password_id']).send_keys(Keys.RETURN)
        print("logged in")
    except:
        print("failed login")
        flash("unable to login")
        pass
    try:
        driver.get(url)
        driver.implicitly_wait(5)
        time.sleep(3)
    except:
        driver.get_screenshot_as_file("000_Clickfail.png")
        flash('Url Not Available')
    sendsnmpagentmessage("noderebootattempt")
    for x in range(1, len(controldata)):
        try:
            print("trying:" ,controldata[x])
            driver.get_screenshot_as_file("capturebefore.png")
            element = driver.find_element(By.XPATH, controldata[x])
            element.click()
            flash("{} {} successfully".format(dname,param), 'success')
            print("clicked")
        except:
            print("failed")
            flash("{} {} failed, check url or xpath in configs".format(dname, param), 'error')
            pass
        try:
            driver.switch_to.alert.accept()
            driver.get_screenshot_as_file("capture{}2.png".format(x))
            flash("{} {} successfully".format(dname, param), 'success')
        except:
            print("exception")
            # flash("{} {} failed, check url or xpath in configs".format(dname, param), 'error')
            driver.get_screenshot_as_file("capture{}3.png".format(x))
            pass

    driver.implicitly_wait(5)
    driver.quit()
    return redirect(url_for("dashboard"))


@app.route('/deviceedit/<cid>', methods=['GET', 'POST'])
@login_required
def device_edit(cid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''

    result2 = session.query(deviceinfo).filter(deviceinfo.id == cid).first()
    session.commit()
    return render_template('deviceedit.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=result2)


@app.route('/editdevice', methods=["GET", "POST"])
@login_required
def editdevice():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if request.method == "POST":
        id = request.form['id']
        device_name = request.form['device_name']
        device_type = request.form['device_type']
        ip = request.form['ip']
        if not ip[-1] == '/':
            ip = ip + '/'
        password = request.form['password']
        username = request.form['username']
        location = request.form['location']
        region = request.form['region']
        description = request.form['description']

        session.query(deviceinfo).filter_by(id=id).update(
            dict(device_name=device_name, region=region,device_type=device_type, ip=ip, password=password, username=username,
                 location=location, description=description))
        session.commit()
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename=device_name, ip=ip,
                         action="A {} type of device with Name {} with IP: {} Details been updated by {}".format(device_type,
                                                                                                          device_name,
                                                                                                          ip,
                                                                                                          current_user.fullname))
        session.add(ins2)
        session.commit()
        flash("updated sucessfully")
        return redirect('/deviceview', code=302)


@app.route('/deviceview')
@login_required
def deviceview():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    disableadd = ""
    result2 = session.query(deviceinfo).order_by(deviceinfo.id.desc()).all()
    session.commit()
    if len(result2) >= licnumdev:
        disableadd = "disabled"      
    if current_user.isadmin != "true":
        result2 = session.query(deviceinfo).filter(deviceinfo.region == current_user.region).order_by(
            deviceinfo.id.desc()).all()
    return render_template('devicemanager.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=result2,
                               active2='active', disableadd=disableadd)



@app.route('/deviceaccess')
@login_required
def deviceaccess():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        result2 = session.query(deviceinfo).order_by(deviceinfo.id.desc()).all()
        session.commit()
    else:
        result2 = session.query(deviceinfo).filter(deviceinfo.region == current_user.region).order_by(deviceinfo.id.desc()).all()
        session.commit()
    return render_template('deviceaccess.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=result2,
                           proxy_url=proxy_url,
                           active11='active')


@app.route('/calculatemos/<ip>')
@login_required
def calculatemos(ip):
    return str(getmosscore(ip))


@app.route('/adddevice', methods=['GET', 'POST'])
@login_required
def adddevice():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    print(current_user.isadmin)
    if current_user.isadmin == "True" or current_user.isadmin == "true":
        if request.method == 'POST':

            numofdevices = len(session.query(deviceinfo).all())
            if numofdevices >= licnumdev:
                print("License limit exceeded. Cannot add more devices")
                flash('Sorry you are licensed to manage '+str(licnumdev)+" devices. You cannot add more devices.")
                return redirect(url_for("deviceview"))
            devicename = request.form['devicename']
            devicetype = request.form['devicetype']
            ip = request.form['ip']
            if not ip[-1] == '/':
                ip = ip+'/'
            if not ip[1] == "h":
                ip = "http://"+ip
            password = request.form['password']
            username = request.form['username']
            region = request.form['region']
            
            g = geocoder.ip('me')
            print(g.latlng)
            location = str(g.lat) + ',' + str(g.lng)
            description = request.form['description']

            ins = deviceinfo(location=location,region=region, description=description, ip=ip, device_type=devicetype,
                             device_name=devicename, username=username,
                             password=password)
            session.add(ins)
            session.commit()

            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename=devicename, ip=ip,
                             action="A {} type of device with Name {} is been added with IP: {} by {}". format(devicetype,devicename,ip,current_user.fullname))
            session.add(ins2)
            session.commit()

            # Adding to SqLite for Uptime Kuma
            try:
                conn = sqlite3.connect(config["DATABASE"]["UPTIMEK"])
                ipaddress = str(ip).replace('http://','')
                ipaddress = ipaddress.replace('/','')
                sql = ''' INSERT INTO monitor (id, name, active, user_id, interval, url, type, weight, hostname)
VALUES (?,?,1,1,60,'https://','ping',2000,?) '''
                print("SQL for SQLite:",sql)
                dbvals=(ins.id,devicename,ipaddress)
                cur = conn.cursor()
                cur.execute(sql,dbvals)
                conn.commit()
                conn.close()

            except Error as e:
                print("Exception in writing to UptimeK DB File")
                print(e)


            flash('Device Added Please Update The Config')
            # exec(open("./chromeheadless.py").read())
            return redirect(url_for("deviceview"))
        else:
            regions = config['REGIONS']['NAMES']
            regions = regions.split(',')
            return render_template("adddevice.html",regions=regions)
    else:
        flash('You Need Admin Privileges')
        return redirect(url_for("dashboard"))


@app.route('/sshdevicecontrol', methods=['GET'])
@login_required
def sshdevicecontrol():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    print(current_user.isadmin)
    if current_user.isadmin == "True" or current_user.isadmin == "true":

       devicename=request.args.get('devicename') 
       command=request.args.get('command').replace('~',' ')
       device = session.query(deviceinfo).filter(deviceinfo.device_name == devicename).first()
       try:
           command = deviceconfig[device.device_type][command]
           print("Parsed command: ",command)
           ipaddress = str(device.ip).replace('http://','')
           ipaddress = ipaddress.replace('/','')
           response = connectviassh(ipaddress, device.username, device.password, command=command)
       except Exception as ee:
           print(ee)
           response = "Error connecting to Node.. Please try later"
       return render_template('sshdevicecontrol.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=response,active11='active')      
    else:
        flash('You Need Admin Privileges')
        return redirect(url_for("dashboard"))

@app.route('/devicedelete/<cid>', methods=['GET', 'POST'])
@login_required
def devicedelete(cid):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "True" or current_user.isadmin == "true":
        devicedetails = session.query(deviceinfo).filter(deviceinfo.id == cid).first()

        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename=devicedetails.device_name, ip=devicedetails.ip,
                         action="A {} type of device with Name {} is been Deleted by {}".format(devicedetails.device_type, devicedetails.device_name, current_user.fullname))
        session.add(ins2)
        session.commit()
        session.query(deviceinfo).filter(deviceinfo.id == cid).delete()
        session.commit()
        flash("Device Deleted Successfully")
        return redirect(url_for("deviceview"))
    else:
        flash('You Need Admin Privileges')
        return redirect(url_for("dashboard"))


@app.route('/userview')
@login_required
def userview():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if current_user.isadmin == "true":
        result2 = session.query(Subscriber).order_by(
            Subscriber.id.desc()).all()
        session.commit()
        return render_template('usermanager.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, data=result2,
                               active3='active')
    else:
        flash('You Need Admin Privileges','error')
        return redirect(url_for("dashboard"))


@app.route('/upload')
def upload():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    return render_template('upload.html')


@app.route('/uploading', methods=['GET', 'POST'])
def uploading():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    print("file submitted")
    if 'file' not in request.files:
        flash('No file part')
        return redirect(request.url)
    file = request.files['file']

    filename = secure_filename(file.filename)
    file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    return render_template('upload.html')


@app.route('/')
def start():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    devices = session.query(
                        deviceinfo).all()
    numdevs = len(devices)
    print("Number of Devices Detected: "+str(numdevs))
    print("Number of Devices Licensed: "+str(licnumdev))
    if numdevs > licnumdev:
        return "System Error. License Violation"
    if current_user.is_authenticated:
        flash('Welcome ' + current_user.fullname.capitalize() + ' !')
        return redirect(url_for('dashboard'))
    else:
        return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    try:
        if not current_user.is_authenticated:
            if request.method == 'POST':
                email = request.form['email']
                password = request.form['password']
                try:
                    subscriber = session.query(
                        Subscriber).filter_by(email=email).first()
                    session.commit()
                except:
                    session.rollback()
                    subscriber = session.query(
                        Subscriber).filter_by(email=email).first()
                    session.commit()

                if subscriber:
                    passdehash = pbkdf2_sha256.verify(
                        password, subscriber.password)
                    passdehash = str(passdehash)

                    if subscriber.email == email and passdehash == 'True':

                        # for newly added user
                        if subscriber.logged_in is None:
                            login_user(subscriber)
                            flask.session['uid'] = uuid.uuid4()
                            users = session.query(Subscriber).filter_by(id=subscriber.id).update(
                                dict(logged_in="True", last_loggedintime=datetime.now(),
                                     session_id=flask.session['_id']))
                        # for user who has logged out of system
                        if subscriber.logged_in == 'False':
                            login_user(subscriber)
                            flask.session['uid'] = uuid.uuid4()
                            users = session.query(Subscriber).filter_by(id=subscriber.id).update(
                                dict(logged_in="True", last_loggedintime=datetime.now(),
                                     session_id=flask.session['_id']))
                        # for user who is already logged_in trying to access from other browser
                        if subscriber.logged_in == 'True':
                            if datetime.now() > subscriber.last_loggedintime + timedelta(hours=1):
                                login_user(subscriber)
                                flask.session['uid'] = uuid.uuid4()
                                users = session.query(Subscriber).filter_by(id=subscriber.id).update(
                                    dict(last_loggedintime=datetime.now(), session_id=flask.session['_id']))
                            else:
                                # print("you are already logged in from different browser window")
                                flash(gettext('you are already logged in from different browser window.'),
                                      category='Fail')
                                return redirect("/login")

                        if current_user.isadmin == "True" or current_user.isadmin == "true":
                            return redirect(url_for('start'))
                        else:
                            return redirect(url_for('cdrparser'))
                    else:
                        sendsnmpagentmessage("invalidemslogin")
                        return render_template('login/login.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                               msg='Incorrect Password')
                else:
                    sendsnmpagentmessage("invalidemslogin")
                    return render_template('login/login.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                           msg='Incorrect Email Or Password')
            return render_template('login/login.html')
        return redirect(url_for('start'))
    except:
        session.rollback()
        email = request.form['email']
        password = request.form['password']

        subscriber = session.query(Subscriber).filter_by(email=email).first()
        session.commit()

        if subscriber:
            passdehash = pbkdf2_sha256.verify(password, subscriber.password)
            passdehash = str(passdehash)

            if subscriber.email == email and passdehash == 'True':
                # login_user(subscriber)
                print("alternative exception ___________*******")
                if subscriber.logged_in is None:
                    login_user(subscriber)
                    flask.session['uid'] = uuid.uuid4()
                    users = session.query(Subscriber).filter_by(id=subscriber.id).update(
                        dict(logged_in="True", last_loggedintime=datetime.now(), session_id=flask.session['_id']))
                if subscriber.logged_in == 'False':
                    login_user(subscriber)
                    flask.session['uid'] = uuid.uuid4()
                    users = session.query(Subscriber).filter_by(id=subscriber.id).update(
                        dict(logged_in="True", last_loggedintime=datetime.now(),
                             session_id=flask.session['_id']))
                if subscriber.logged_in == 'True':
                    if datetime.now() > subscriber.last_loggedintime + timedelta(hours=1):
                        login_user(subscriber)
                        flask.session['uid'] = uuid.uuid4()
                        users = session.query(Subscriber).filter_by(id=subscriber.id).update(
                            dict(last_loggedintime=datetime.now(), session_id=flask.session['_id']))
                    else:
                        print("you are already logged in from different browser window")
                        flash(gettext('you are already logged in from different browser window.'),
                              category='Fail')
                        return redirect("/login")

                if current_user.isadmin == "True" or current_user.isadmin == "true":
                    return redirect(url_for('start'))
                else:
                    return redirect(url_for('cdrparser'))
            else:
                sendsnmpagentmessage("invalidemslogin")
                return render_template('login/login.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                       msg='Incorrect Password')
        else:
            sendsnmpagentmessage("invalidemslogin")
            return render_template('login/login.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                   msg='Incorrect Email Or Password')


# for flicket user creation
def create_user(username, password, email=None, name=None, job_title=None, locale=None, disabled=None):
    password = hash_password(password)
    register = FlicketUser(username=username,
                           email=email,
                           name=name,
                           password=password,
                           job_title=job_title,
                           date_added=datetime.now(),
                           locale=locale,
                           disabled=disabled)
    db.session.add(register)
    db.session.commit()
    if job_title == 'admin':
        create_admin_group(username=username)


def create_admin_group(username, silent=False):
    print("inside create admin")
    print(username)
    """ creates flicket_admin and super_user group and assigns flicket_admin to group admin. """
    try:
        query = FlicketGroup.query.filter_by(group_name=app.config['ADMIN_GROUP_NAME'])
        if query.count() == 0:
            add_group = FlicketGroup(group_name=app.config['ADMIN_GROUP_NAME'])
            db2.session.add(add_group)
            if not silent:
                print("Admin group added")

        user = FlicketUser.query.filter_by(username=username).first()
        group = FlicketGroup.query.filter_by(group_name=app.config['ADMIN_GROUP_NAME']).first()
        in_group = False
        # see if user flicket_admin is already in flicket_admin group.
        for g in group.users:
            print(g.username)
            if g.username == username:
                in_group = True
                break
        if not in_group:
            group.users.append(user)
            # db2.session.commit()
            if not silent:
                print("Added flicket_admin user to flicket_admin group.")

        #  create the super_user group
        query = FlicketGroup.query.filter_by(group_name=app.config['SUPER_USER_GROUP_NAME'])
        if query.count() == 0:
            add_group = FlicketGroup(group_name=app.config['SUPER_USER_GROUP_NAME'])
            db2.session.add(add_group)
            if not silent:
                print("super_user group added")

    except (IntegrityError, FlushError) as e:
        print("%$%#%%$#%$#%#%#    EXCEPTION %#$@#@U$")
        db2.rollback()
        db2.session.rollback()
        raise print(*e.args)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if request.method == 'POST':
        fullname = request.form['first_name']+request.form['last_name']
        region = request.form.get("region", None)
        email = request.form['email']
        mobile = request.form['mobile']
        password = request.form['password']
        isadmin = request.form['isadmin']
        readonly = request.form['readonly']

        print(fullname,region,isadmin,readonly)
        passhash = pbkdf2_sha256.hash(password)
        print("username is {}".format(fullname + str(mobile)[5:10]))
        if isadmin == 'true':
            job_title = "admin"
        else:
            job_title = "user"

        success_check = 0
        try:
            subscriber = session.query(
                Subscriber).filter_by(mobile=mobile).first()
            if subscriber:
                success_check = 0
                return render_template('login/register.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                       msg='MobileNo Already Used')
            else:
                success_check = 1

            subscriber = session.query(
                Subscriber).filter_by(email=email).first()
            if subscriber:
                success_check = 0
                return render_template('login/register.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                       msg='Email already registered')
            else:
                success_check = 1
        except:
            session.rollback()
            subscriber = session.query(
                Subscriber).filter_by(mobile=mobile).first()
            if subscriber:
                success_check = 0
                return render_template('login/register.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                       msg='MobileNo Already Used')
            else:
                success_check = 1

            subscriber = session.query(
                Subscriber).filter_by(email=email).first()
            if subscriber:
                success_check = 0
                return render_template('login/register.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost,
                                       msg='Email already registered')
            else:
                success_check = 1

        new_user = Subscriber(fullname=fullname, email=email,
                              password=passhash, mobile=mobile,region=region,readonly=readonly, isadmin=isadmin)
        session.add(new_user)
        session.commit()
        sendsnmpagentmessage("newemsuseradded")

        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='UserManager',
                         action="A user {} is added with isadmin as {} by {}.".format(fullname,isadmin,current_user.fullname))
        session.add(ins2)
        session.commit()

        if success_check == 1:
            create_user(fullname + str(mobile)[5:10],
                        password,
                        email=email,
                        name=fullname,
                        job_title=job_title,
                        locale='en',
                        disabled=0)

        global subscriber2
        b = 0

        subscribers = session.query(Subscriber).all()
        for subscriber1 in subscribers:
            b = b + 1

        if isadmin == 'true':
            useris = 'Admin'
        else:
            useris = 'User'

        return redirect(url_for('userview'))

    regions = config['REGIONS']['NAMES']
    regions = regions.split(',')
    return render_template('login/register.html',regions=regions)


@app.route('/logout')
@login_required
def logout():
    print("logged out")
    try:
        g.user.revoke_token()
        # db.session.commit()
        db2.session.commit()
    except:
        pass
    id = current_user.id

    print(session.query(Subscriber).filter_by(id=id).update(dict(logged_in="False")))
    logout_user()
    del flask.session['uid']
    flash(gettext('You were logged out successfully.'), category='success')
    return redirect("/login")


@app.route('/backup')
@login_required
def backupfunc():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    path = backup()
    user = current_user

    return render_template('profile.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, user=user, path12=path)


@app.route('/profile')
@login_required
def profile():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    user = current_user

    return render_template('profile.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, user=user)


@app.route('/editprofile', methods=["GET", "POST"])
@login_required
def editprofile():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    if request.method == "POST":
        id = request.form['id']
        fullname = request.form['name']
        email = request.form['email']
        mobile = request.form['mobile']
        users = session.query(Subscriber).filter_by(id=id).update(
            dict(fullname=fullname, email=email, mobile=mobile))
        session.commit()
        user = session.query(Subscriber).filter_by(id=id).first()

        session.commit()
        return render_template('profile.html', subcdr_menu=subcdr_menu, myurlhost=myurlhost, user=user, updated='yes')


@app.route('/admindelete/<uid>/<type>')
@login_required
def admindelete(uid,type):
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    try:
        new = session.query(Subscriber).filter_by(id=uid).first()
        # Activity LOG AUDIT
        ins2 = Auditlogs(devicename='UserManager',
                         action="A user {} is removed by {}.".format(new.fullname, current_user.fullname))
        session.add(ins2)
        session.commit()

        session.delete(new)
        session.commit()
        # logout_user()

        if type == 'a':
            return redirect(url_for('login'))
        else:
            return redirect(url_for('userview'))
    except UnmappedInstanceError:
        session.rollback()
        return redirect(url_for('logout'))


@app.route('/search', methods=['GET', 'POST'])
def search():
    if request.is_secure:
        myurlhost = 'https://' + request.host
    else:
        myurlhost = ''
    result = request.form['column_names']
    if result == 'home':
        return redirect(url_for('dashboard'))

    if result == 'add device':
        return redirect(url_for('adddevice'))

    if result == 'devices':
        return redirect(url_for('deviceview'))

    if result == 'users':
        return redirect(url_for('userview'))

    if result == '':
        return redirect('start')
    else:
        return redirect('start')


@login_manager.user_loader
def load_user(user_id):
    return session.query(Subscriber).get(int(user_id))


@login_manager.unauthorized_handler
def userunauth():
    flash('Unauthorized User', 'info')
    return redirect(url_for('start'))


@app.errorhandler(404)
def not_found(e):
    return render_template("errors/404.html")


# handle unexpected errors

@app.errorhandler(403)
def not_found_error(error):
    return render_template('403.html'), 403


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500

@app.template_filter('format_datetime')
def format_datetime(value):
    return value.total_seconds() / 3600

@app.context_processor
def inject_stage_and_region():
    return dict(swversion=swversion)

def sendsnmpagentmessage(message):
    try:
        r = None
        if snmpagentenable:
            if config['REDIS']['PASSWORD']!="":
                r = redis.Redis(host=config['REDIS']['HOST'],port=int(config['REDIS']['PORT']), password=config['REDIS']['PASSWORD'])    
            else:
                r = redis.Redis(host=config['REDIS']['HOST'],port=int(config['REDIS']['PORT']))
        if snmpagentenable and r is not None:
            r.publish(channel="outgoingsnmpagent", message = pickle.dumps([message]))
            print(f"Sending {message} to snmpagent")
            r.close()
    except Exception as ee:
        print(ee)
        print("Cannot send message to snmpagent")



if __name__ == "__main__":
    # print(pyfiglet.figlet_format("emsNEXT - Telenext"))
    # print(pyfiglet.figlet_format("v2.3.1", font = "bubble" ))
    print("|=====================================================|")
    print("| emsNEXT - Telenext                                  |")
    print(f"| {swversion} ({swdate})                               |")
    print("| Built by Telenext Systems. www.telenextsystems.com  |")
    print("| Please contact samrat@telenextsystems.com           |")
    print("|=====================================================|")
    numdevices = license.licenseCheck()
    # numdevices = 1

    try:
        licnumdev = int(numdevices)
        print("Ems license for number of devices: "+numdevices)
    except:
        print("Invalid number of licensed devices. Exiting")
        sys.exit(255)
    if flaskdebug == 1:
        app.run(debug=True, port=sysport, use_reloader=True, use_debugger=True)
    else:
        serve(app, listen='*:'+str(sysport))