------------------------------------------------------------------------------
------------------------------------------------------------------------------
--
-- File         : ems_v2.mib
-- Description  : Telenext EMS Enterprise MIB 
-- $Revision: 1.0 $  
-- $Date: 2022/06/04 17:30:26IST  *
--
-- Copyright (c) 2022 www.telenextsystems.com .  All Rights Reserved.
--
--
------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------
------------------------------------------------------------------------------
EMS-MIB DEFINITIONS ::= BEGIN

	IMPORTS
	    MODULE-IDENTITY, OBJECT-TYPE, NOTIFICATION-TYPE,
	    TimeTicks, enterprises, Counter32			FROM SNMPv2-SMI
	    DisplayString, TestAndIncr, TimeStamp			FROM SNMPv2-TC
	    OBJECT-GROUP, NOTIFICATION-GROUP			FROM SNMPv2-CONF;


	telenext OBJECT IDENTIFIER ::= { enterprises 58999 }

	ems OBJECT IDENTIFIER ::= { telenext 1 }
	
	 emsnotificationsgroup MODULE-IDENTITY
	    LAST-UPDATED "202206040000Z"
	    ORGANIZATION "www.telenextsystems.com"
	    CONTACT-INFO    
		 "postal:   Telenext Systems Pvt. Ltd
		 Hyderabad India

	          email:    support@telenextsystems.com"
	    DESCRIPTION
		"Notification MIB objects for SNMP v3 implementations"
	    REVISION     "202206040000Z"
	    DESCRIPTION
		"notification definitions"
	    REVISION     "202206040000Z"
	    DESCRIPTION
		"Initial Version"
	    ::= { ems 1 }

	
	emsnotifications OBJECT IDENTIFIER ::= { emsnotificationsgroup 1 }
	emsnotificationsPrefix  OBJECT IDENTIFIER ::= { emsnotifications 0 }
	emsSnmpGroups           OBJECT IDENTIFIER ::= {ems 2}


		
	invalidemslogin  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" Attempt to login to the ems server with invalid credentials "
		::= {emsnotificationsPrefix  1}

	
	unreachablenode  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" One or more nodes are not reachable "
		::= {emsnotificationsPrefix  2}


	nodescannererror  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" The EMS scanner has faced issues whle scanning the nodes "
		::= {emsnotificationsPrefix  3}
	
	noderebootattempt  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" EMS operator has attempted a node reboot or shutdown from EMS interface "
		::= {emsnotificationsPrefix  4}
	
	noderestoration  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" EMS operator has attempted to restore a node from EMS interface "
		::= {emsnotificationsPrefix  5}

	newemsuseradded  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" Attempt to login to the ems server with invalid credentials "
		::= {emsnotificationsPrefix  6}
	
	firmupdatejobstarted  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" EMS operator has started a firmware update process on a node"
		::= {emsnotificationsPrefix  7}
	
	highcpuusage  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" CPU usage very high. Triggered at > 80% "
		::= {emsnotificationsPrefix  8}
	
	highmemoryusage  NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" RAM usage is very high. Triggered at > 80% "
		::= {emsnotificationsPrefix  9}

	lowdiskspacewarning 
		NOTIFICATION-TYPE
		STATUS		current
		DESCRIPTION  	" EMS Server has low disk space "
		::= {emsnotificationsPrefix  10}



	emsSnmpNotificationGroups NOTIFICATION-GROUP
	    NOTIFICATIONS { invalidemslogin, unreachablenode, nodescannererror, noderebootattempt, noderestoration, newemsuseradded, firmupdatejobstarted, highcpuusage, highmemoryusage, lowdiskspacewarning }
	    STATUS	current
	    DESCRIPTION
		"The notifications relating to the basic operation of the EMS system."
	    ::= { emsSnmpGroups 1 }
	
	
	       


	-- End of EMS - MIB
END

