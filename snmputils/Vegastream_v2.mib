------------------------------------------------------------------------------
------------------------------------------------------------------------------
--
-- File         : Vegastream.mib
-- Description  : Vegastream Enterprise MIB 
-- $Revision: 1.3 $  
-- $Date: 2010/06/04 17:30:26BST $ $Author: harwoo_t $ $State: dev $ *
--
-- Copyright (c) 2004 Vegastream.  All Rights Reserved.
--
--
------------------------------------------------------------------------------
--
--
------------------------------------------------------------------------------
------------------------------------------------------------------------------
Vega-MIB DEFINITIONS ::= BEGIN

	IMPORTS
	    MODULE-IDENTITY, OBJECT-TYPE, NOTIFICATION-TYPE,
	    TimeTicks, enterprises, Counter32			FROM SNMPv2-SMI
	    DisplayString, TestAndIncr, TimeStamp			FROM SNMPv2-TC
	    OBJECT-GROUP, NOTIFICATION-GROUP			FROM SNMPv2-CONF;


	--  Vega MIB for Vegastream gateway products 


	-- vegastream (Vegastream Enterprise) Enterprise MIB Extensions
	vegastream OBJECT IDENTIFIER ::= { enterprises 4686 }
	

	-- Vegastream platform
	vsplatform OBJECT IDENTIFIER ::= { vegastream 11 }
	
 	-- Vegastream Call Statistics
	callStats OBJECT IDENTIFIER ::= { vsplatform 1 }
    	
 	-- Vegastream Interfaces
	interfaces OBJECT IDENTIFIER ::= { vsplatform 2 }  
	
 	-- Vegastream Notifications
	 vegastreamnotifications MODULE-IDENTITY
	    LAST-UPDATED "201006040000Z"
	    ORGANIZATION "www.vegastream.com"
	    CONTACT-INFO    
		 "postal:   VegaStream Group Ltd
	                  Asmec Centre
	                  Eagle House
	                  The Ring
	                  Bracknell  RG12 1HB

	          email:    support@vegastream.com"
	    DESCRIPTION
		"Notification MIB objects for SNMP v3 implementations"
	    REVISION     "201006040000Z"
	    DESCRIPTION
		"notification definitions"
	    REVISION     "201006040000Z"
	    DESCRIPTION
		"Updated Version"
	    ::= { vsplatform 3 }


	-- Vegastream Call Statistics Objects are defined below

	-- System up time

	noCircuits	OBJECT-TYPE
		SYNTAX  INTEGER
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of circuits on all interfaces"
		::=  { callStats 1 } 	

	upTime		OBJECT-TYPE
		SYNTAX  INTEGER
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total up time of all interfaces"
		::=  { callStats 2 } 

	downTime 	OBJECT-TYPE
		SYNTAX  INTEGER
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total down time of all interfaces"
		::=  { callStats 3 }

	startTime	OBJECT-TYPE
		SYNTAX  INTEGER
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Start time of the last interface"
		::=  { callStats 4 }

	endTime	OBJECT-TYPE
		SYNTAX  INTEGER
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" End time of the last interface"
		::=  { callStats 5 } 

	inboundCalls	OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of inbound calls"
		::=  { callStats 6 }

	inboundAnswered	OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of inbound calls answered"
		::=  { callStats 7 }

	inboundBusy	OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of inbound calls that were busy "
		::=  { callStats 8 }

	inboundNoAnswer	OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of inbound calls not answered"
		::=  { callStats 9 }

	inboundTermReject OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of inbound calls rejected by terminal"
		::=  { callStats 10 }     

	outboundCalls	OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of outbound calls"
		::=  { callStats 11 }

	outboundAnswered OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of outbound calls answered"
		::=  { callStats 12 }

	outboundBusy	OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of outbound calls that were busy"
		::=  { callStats 13 }

	outboundNoAnswer OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of outbound calls not answered"
		::=  { callStats 14 }

	outboundTermReject OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Total number of outbound calls rejected by terminal"
		::=  { callStats 15 }  

	inboundCurrUse OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Current number of inbound calls "
		::=  { callStats 16 }

	outboundCurrUse OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Current number of outbound calls "
		::=  { callStats 17 }

	inboundMaxUse OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum Number of inbound calls "
		::=  { callStats 18 } 

	outboundMaxUse OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum Number of outbound calls "
		::=  { callStats 19 }                 

	allMaxUse OBJECT-TYPE
		SYNTAX  Counter32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum Number of inbound & outbound calls "
		::=  { callStats 20 }

	inboundAvAnswer OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average answer time for inbound calls "
		::=  { callStats 21 }

	outboundAvAnswer OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average answer time for outbound calls "
		::=  { callStats 22 }

	inboundAvCall OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average call time for inbound calls "
		::=  { callStats 23 }

	inboundMaxCall OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum call time for an inbound call "
		::=  { callStats 24 }

	outboundAvCall OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average call time for outbound calls "
		::=  { callStats 25 }

	outboundMaxCall OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum call time for an outbound call "
		::=  { callStats 26 }

	inboundAvDialSucc OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average dial to answer time for inbound calls "
		::=  { callStats 27 }

	outboundAvDialSucc OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average dial to answer time for outbound calls "
		::=  { callStats 28 }          

	inboundAvDialFail OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average dial to fail time for inbound calls "
		::=  { callStats 29 }	

	outboundAvDialFail OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average dial to fail time for outbound calls "
		::=  { callStats 30 }
	
	averageJitter OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average jitter across interfaces "
		::=  { callStats 31 }

	maximumJitter OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum jitter across interfaces "
		::=  { callStats 32 }

	averageLoss OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Average loss across interfaces "
		::=  { callStats 33 }

	maximumLoss OBJECT-TYPE
		SYNTAX  Integer32
		ACCESS  read-only
		STATUS  mandatory
		DESCRIPTION  	" Maximum loss across interfaces "
		::=  { callStats 34 }


	-- Vegastream interfaces
	interfaces OBJECT IDENTIFIER ::= { vsplatform 2 }  

	ifNumber	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of network interfaces (regardless of their
            	current state) present on this system."
    		::= { interfaces 1 }

	InterfaceIndex ::= TEXTUAL-CONVENTION
    		DISPLAY-HINT "d"
    		STATUS       current
    		DESCRIPTION
            	"A unique value, greater than zero, for each interface or
            	interface sub-layer in the managed system.  It is
            	recommended that values are assigned contiguously starting
            	from 1.  The value for each interface sub-layer must remain
            	constant at least from one re-initialization of the entity's
            	network management system to the next re-initialization."
    		SYNTAX       Integer32 (1..2147483647)

	-- the Interfaces table
	-- The Interfaces table contains information on the entity's
	-- interfaces.  Each sub-layer below the internetwork-layer
	-- of a network interface is considered to be an interface.
	ifTable OBJECT-TYPE
   		SYNTAX      SEQUENCE OF IfEntry
    		MAX-ACCESS  not-accessible
    		STATUS      current
    		DESCRIPTION
            	"A list of interface entries.  The number of entries is
            	given by the value of ifNumber."
    		::= { interfaces 2 }

	ifEntry OBJECT-TYPE
    		SYNTAX      IfEntry
    		MAX-ACCESS  not-accessible
    		STATUS      current
    		DESCRIPTION
            	"An entry containing management information applicable to a
           	 particular interface."
    		INDEX   { ifIndex }
    		::= { ifTable 1 }

	IfEntry ::= SEQUENCE {
		ifIndex 			InterfaceIndex,
		ifType				Integer32,
		ifNoCircuits		Integer32,
		ifUpTime			Integer32,
		ifDownTime			Integer32,
		ifStartTime			Integer32,
		ifEndTime			Integer32,
		ifInboundCalls		Counter32,
		ifInboundAnswered 	Counter32,
		ifInboundBusy  		Counter32,
		ifInboundNoAnswer 	Counter32,
		ifInboundTermReject	Counter32,
		ifOutboundCalls	 	Counter32,
		ifOutboundAnswered 	Counter32,
		ifOutboundBusy	 	Counter32,
		ifOutboundNoAnswer 	Counter32,
		ifOutboundTermReject 	Counter32,
		ifInboundCurrUse  	Counter32,
		ifOutboundCurrUse  	Counter32,
		ifInboundMaxUse 		Counter32,
		ifOutboundMaxUse 		Counter32,
		ifAllMaxUse 		Counter32,
		ifInboundAvAnswer  	Integer32,
		ifOutboundAvAnswer 	Integer32,
		ifInboundAvCall  		Integer32,
		ifInboundMaxCall  	Integer32,
		ifOutboundAvCall  	Integer32,
		ifOutboundMaxCall  	Integer32,
		ifInboundAvDialSucc 	Integer32,
 		ifOutboundAvDialSucc 	Integer32,
		ifInboundAvDialFail 	Integer32,
		ifOutboundAvDialFail 	Integer32,
		ifAverageJitter  		Integer32,
		ifMaximumJitter  		Integer32,
		ifAverageLoss  		Integer32,
		ifMaximumLoss   		InterfaceIndex
    		}

	ifIndex OBJECT-TYPE
    		SYNTAX      InterfaceIndex
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"A unique value, greater than zero, for each interface.  It
            	is recommended that values are assigned contiguously
            	starting from 1.  The value for each interface sub-layer
            	must remain constant at least from one re-initialization of
            	the entity's network management system to the next re-
            	initialization."
    		::= { ifEntry 1 }

	ifType	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The type of the interface"
    		::= { ifEntry 2 }

	ifNoCircuits	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of circuits on the interface"
    		::= { ifEntry 3 }

	ifUpTime	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The up time of the Interface in seconds"
    		::= { ifEntry 4 }

	ifDownTime	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The down time of the Interface in seconds"
    		::= { ifEntry 5 }

	ifStartTime	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The Start time of the Interface in seconds"
    		::= { ifEntry 6 }

	ifEndTime	OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The End time of the Interface in seconds"
    		::= { ifEntry 7 }

	ifInboundCalls	OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of inbound calls for the Interface"
    		::= { ifEntry 8 }
	
	ifInboundAnswered OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of inbound calls answered for the Interface"
    		::= { ifEntry 9 }

	ifInboundBusy OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of inbound calls busy for the Interface"
    		::= { ifEntry 10 }

	ifInboundNoAnswer OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of inbound calls not answered for the Interface"
    		::= { ifEntry 11 }

	ifInboundTermReject OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of inbound calls rejected on the terminal for the Interface"
    		::= { ifEntry 12 }

	ifOutboundCalls	OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of outbound calls for the Interface"
    		::= { ifEntry 13 }

	ifOutboundAnswered	OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of outbound calls answered for the Interface"
    		::= { ifEntry 14 }

	ifOutboundBusy	OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of outbound calls busy for the Interface"
    		::= { ifEntry 15 }

	ifOutboundNoAnswer	OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of outbound calls not answered for the Interface"
    		::= { ifEntry 16 }

	ifOutboundTermReject OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The number of outbound calls rejected on the terminal for the Interface"
    		::= { ifEntry 17 }

	ifInboundCurrUse OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The current number of inbound calls for the Interface"
    		::= { ifEntry 18 }

	ifOutboundCurrUse OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The current number of outbound calls for the Interface"
    		::= { ifEntry 19 }

	ifInboundMaxUse OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum number of inbound calls for the Interface"
    		::= { ifEntry 20 }

	ifOutboundMaxUse OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum number of outbound calls for the Interface"
    		::= { ifEntry 21 }

	ifAllMaxUse OBJECT-TYPE
    		SYNTAX      Counter32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum number of inbound & outbound calls for the Interface"
    		::= { ifEntry 22 }

	ifInboundAvAnswer OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average answer time of inbound calls for the Interface"
    		::= { ifEntry 23 } 

	ifOutboundAvAnswer OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average answer time of outbound calls for the Interface"
    		::= { ifEntry 24 } 

	ifInboundAvCall OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average call time of inbound calls for the Interface"
    		::= { ifEntry 25 } 

	ifInboundMaxCall OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum call time of an inbound call for the Interface"
    		::= { ifEntry 26 }

	ifOutboundAvCall OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average call time of outbound calls for the Interface"
    		::= { ifEntry 27 } 

	ifOutboundMaxCall OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum call time of an outbound call for the Interface"
    		::= { ifEntry 28 }

	ifInboundAvDialSucc OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average dial to answer time of inbound calls for the Interface"
    		::= { ifEntry 29 }
 
 	ifOutboundAvDialSucc OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average dial to answer time of outbound call for the Interface"
    		::= { ifEntry 30 }  

	ifInboundAvDialFail OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average dial to fail time of inbound calls for the Interface"
    		::= { ifEntry 31 } 

	ifOutboundAvDialFail OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average dial to fail time of outbound calls for the Interface"
    		::= { ifEntry 32 } 

	ifAverageJitter OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average jitter of incoming packets for the Interface"
    		::= { ifEntry 33 }

	ifMaximumJitter OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum jitter of incoming packets for the Interface"
    		::= { ifEntry 34 }

	ifAverageLoss OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The average loss of incoming packets for the Interface"
    		::= { ifEntry 35 } 


	ifMaximumLoss OBJECT-TYPE
    		SYNTAX      Integer32
    		MAX-ACCESS  read-only
    		STATUS      current
    		DESCRIPTION
            	"The maximum loss of incoming packets for the Interface"
    		::= { ifEntry 36 }    
	
	-- Vegastream Notifications
	
	vegaNotifications OBJECT IDENTIFIER ::= { vegastreamnotifications 1 }
	vegaNotificationPrefix  OBJECT IDENTIFIER ::= { vegaNotifications 0 }

	--
	--  Conformance
	--

	vegaConformance          OBJECT IDENTIFIER ::= {vegastream 5}
	vegaCompliances          OBJECT IDENTIFIER ::= {vegaConformance 1}
	vegaSnmpGroups           OBJECT IDENTIFIER ::= {vegaConformance 2}

		
	fileServerOrFileNotFound  NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE    	vsplatform
		DESCRIPTION  	" File server for autoexec not found.
		TFTP/FTP/HTTP/HTTPS server not found - it might not be
		running or cannot be reached over the LAN; Or the requested
		file is not available / defined on the server "
		::= {vegaNotificationPrefix  1}

	scriptFileTooBig  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Autoexec script file is too big; there is a 512 byte 
		limit on the script file "
		::= {vegaNotificationPrefix  2}

	fileServerUnknownError  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" FileServer (TFTP/FTP/HTTP/HTTPS) unknown error reported "
		::= {vegaNotificationPrefix  3}

	fileDoesNotExist  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" File does not exist on FileServer or invalid filename "
		::= {vegaNotificationPrefix  4}

	recurFileServerGetError  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Recursive FileServer (TFTP/FTP/HTTP/HTTPS) get error -
		only allowed one TFTP/FTP/HTTP/HTTPS get at a time "
		::= {vegaNotificationPrefix  5}

	fileServerGetMemoryError  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Get memory error - the system hasn't 
		enough memory to perform a TFTP/FTP/HTTP/HTTPS get "
		::= {vegaNotificationPrefix  6}

	lanInterfaceNotActive  NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Lan Interface Entity not active "
		::= {vegaNotificationPrefix  7}

	ftpNotConfigured  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" FTP not configured - FTP IP address is set to 0.0.0.0 0 "
		::= {vegaNotificationPrefix  8}

	invalidUserParams  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Invalid user parameters e.g. incorrect filename"
		::= {vegaNotificationPrefix  9}

	serverUnavailable  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" FTP/TFTP/HTTP/HTTPS Server Unavailable "
		::= {vegaNotificationPrefix  10}

	systemNotReady  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" System not ready - unable to action AUTOEXEC "
		::= {vegaNotificationPrefix  11}

	tftpNotConfigured	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE 		vsplatform
		DESCRIPTION  	" TFTP not configured - TFTP IP address is set to 0.0.0.0 "
		::= {vegaNotificationPrefix  12}

	httpNotConfigured	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE 		vsplatform
		DESCRIPTION  	" HTTP not configured - HTTP IP address is set to 0.0.0.0 "
		::= {vegaNotificationPrefix  13}

	httpsNotConfigured	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE 		vsplatform
		DESCRIPTION  	" HTTPS not configured - HTTPS IP address is set to 0.0.0.0 "
		::= {vegaNotificationPrefix  14}

	-- Autoexec load Non-error status

	configNotLoaded  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Configuration not loaded tget used with ifdiff option but remote configuration file is not different "
		::= {vegaNotificationPrefix  21}

	firmwareNotLoaded  		NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Firmware not loaded, one of the following occurred
		i. download firmware used with ifnew option but remote firmware is not newer
		ii.download firmware used with ifdiff option but remote firmware is not different "
		::= {vegaNotificationPrefix  22}

	configLoaded  		NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Configuration loaded- loaded remote configuration "
		::= {vegaNotificationPrefix  23}

	firmwareLoaded  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Firmware loaded - loaded remote firmware "
		::= {vegaNotificationPrefix  24}


	-- 8 FXS + 2 FXO - Fallback relay state
	
	bypassRelayActivated  	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Ongoing Emergency Call on port 1 or 2 POTS bypass relay activated "
		::= {vegaNotificationPrefix  25}	

	-- QOS statistics

	pktLossThresholdExceed	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Gateway packet loss threshold has been exceeded "
		::= {vegaNotificationPrefix  30}

	playoutThresholdExceed	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Gateway play-out delay threshold has been exceeded "
		::= {vegaNotificationPrefix  31}

	jitterThresholdExceed	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Average jitter threshold has been exceeded "
		::= {vegaNotificationPrefix  32}

	-- Thermal Control traps

	sysFanFailed  		NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" All System Fans Failed "
		::= {vegaNotificationPrefix  40}

	sysFanOK  		NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" System Fan OK "
		::= {vegaNotificationPrefix  41}

	sysOverTempIndication	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Over Temperature Indication "
		::= {vegaNotificationPrefix  42}

	sysOverTempOK	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Over Temperature OK "
		::= {vegaNotificationPrefix  43}

	sysOverPower	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Excessive Power Consumption "
		::= {vegaNotificationPrefix  44}

	sysOverPowerOK	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Power Usage OK "
		::= {vegaNotificationPrefix  45}

	fxsPortShutDown 	NOTIFICATION-TYPE
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" FXS Port Shut down due to thermal heating "
		::= {vegaNotificationPrefix  46}

	-- Sip Registration traps

	sipRegistrationSuccess	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Sip Registration Success Indication "
		::=  {vegaNotificationPrefix  50} 

	sipRegistrationFailure	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Sip Registration Failure Indication "
		::= {vegaNotificationPrefix  51} 

	sipUnRegistered 	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" Sip Unregistered Indication "
		::= {vegaNotificationPrefix  52} 

	-- ISDN Link Status traps	

	iSDNLinkUp	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" ISDN Link up "
		::=  {vegaNotificationPrefix  60} 

	iSDNLinkDown	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" ISDN Link down "
		::=  {vegaNotificationPrefix  61}    


	-- LAN Link Status traps	         

	lANLinkUp	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" LAN Link up "
		::=  {vegaNotificationPrefix  62} 

	lANLinkDown	NOTIFICATION-TYPE
		OBJECTS		{ sysDescr }
		STATUS		current
	--	ENTERPRISE   	vsplatform
		DESCRIPTION  	" LAN Link down "
		::=  {vegaNotificationPrefix  63}

	--
	-- Conformance-related definitions
	--


	vegaSnmpNotificationGroups NOTIFICATION-GROUP
	    NOTIFICATIONS { fileServerOrFileNotFound, scriptFileTooBig, fileServerUnknownError,
				fileDoesNotExist,recurFileServerGetError,fileServerGetMemoryError,
	    			lanInterfaceNotActive,ftpNotConfigured,invalidUserParams,serverUnavailable,
	    			systemNotReady,tftpNotConfigured,httpNotConfigured,httpsNotConfigured,
				configNotLoaded,firmwareNotLoaded,configLoaded,firmwareLoaded,bypassRelayActivated,
				pktLossThresholdExceed,playoutThresholdExceed,jitterThresholdExceed,sysFanFailed,
	  			sysFanOK,sysOverTempIndication,sysOverTempOK,sysOverPower,fxsPortShutDown,sipRegistrationSuccess,
				sipRegistrationFailure,sipUnRegistered,iSDNLinkUp,iSDNLinkDown,lANLinkUp,lANLinkDown }
	    STATUS	current
	    DESCRIPTION
		"The notifications relating to the basic operation of the vega system."
	    ::= { vegaSnmpGroups 1 }
	       


	-- End of Vega - MIB
END

