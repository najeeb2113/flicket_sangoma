from selenium import webdriver

from selenium.webdriver.chrome.options import Options
import os

import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilename()
print(file_path)

chrome_options = Options()
chrome_options.add_argument("--headless")
# chrome_options.add_argument("--window-size=1920x1080")

chrome_options.add_argument("window-size=1200x600")

# current directory
chrome_driver = os.getcwd() +"\\chromedriver.exe"

# driver = webdriver.Chrome(chrome_options=chrome_options, executable_path=chrome_driver)
driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
driver.get("http://127.0.0.1:5034/upload")


elm = driver.find_element_by_xpath("//input[@type='file']")
elm.send_keys(os.getcwd() + "\\config.ini")
# elm.send_keys(file_path)


# elm = driver.find_element_by_xpath("//input[@type='file']").click()  # Example xpath


driver.get_screenshot_as_file("capture.png")

driver.find_element_by_xpath("//button[@type='submit']").click()

driver.implicitly_wait(10)
driver.get_screenshot_as_file("afterloginimg.png")

pagename = driver.title

driver.quit()