from pprint import pprint
import redis
import pickle
import configparser
import os
from threading import Thread
import time
import psutil
import shutil

txtconfig = configparser.ConfigParser()
txtconfig.read('config.ini')
# from pysnmp.hlapi import *
#import subprocess

manager = txtconfig['SNMPAGENT']['MANAGER']
community = txtconfig['SNMPAGENT']['COMMUNITY']
area = txtconfig['SNMPAGENT']['AREA']
cpulimit = int(txtconfig['SNMPAGENT']['CPULIMIT'])
memlimit = int(txtconfig['SNMPAGENT']['MEMLIMIT'])
dflimit = int(txtconfig['SNMPAGENT']['DFLIMIT'])

oidmap={}
oidmap["invalidemslogin"]="1.3.6.1.4.1.58999.1.1.1.0.1"
oidmap["unreachablenode"]="1.3.6.1.4.1.58999.1.1.1.0.2"
oidmap["nodescannererror"]="1.3.6.1.4.1.58999.1.1.1.0.3"
oidmap["noderebootattempt"]="1.3.6.1.4.1.58999.1.1.1.0.4"
oidmap["noderestoration"]="1.3.6.1.4.1.58999.1.1.1.0.5"
oidmap["newemsuseradded"]="1.3.6.1.4.1.58999.1.1.1.0.6"
oidmap["firmupdatejobstarted"]="1.3.6.1.4.1.58999.1.1.1.0.7"
oidmap["highcpuusage"]="1.3.6.1.4.1.58999.1.1.1.0.8"
oidmap["highmemoryusage"]="1.3.6.1.4.1.58999.1.1.1.0.9"
oidmap["lowdiskspacewarning"]="1.3.6.1.4.1.58999.1.1.1.0.10"




if txtconfig['REDIS']['PASSWORD']!="":
    r = redis.Redis(
    host=txtconfig['REDIS']['HOST'],
    port=int(txtconfig['REDIS']['PORT']), password=txtconfig['REDIS']['PASSWORD'])
else:
    r = redis.Redis(
    host=txtconfig['REDIS']['HOST'],
    port=int(txtconfig['REDIS']['PORT']))

# def sendtrap(trapid):
#     iterator = sendNotification(
#         SnmpEngine(),
#         CommunityData('public', mpModel=0),
#         UdpTransportTarget(('localhost', 162)),
#         ContextData(),
#         'trap',
#         NotificationType(
#             ObjectIdentity('1.3.6.1.4.1.58999.1.1.1.0.2')
#         ).addVarBinds(
#             ('1.3.6.1.6.3.1.1.4.3.0', '1.3.6.1.4.1.20408.4.1.1.2'),
#             ('1.3.6.1.2.1.1.1.0', OctetString('my system'))
#         ).loadMibs(
#             'SNMPv2-MIB'
#         )
#     )

#     errorIndication, errorStatus, errorIndex, varBinds = next(iterator)

#     if errorIndication:
#         print(errorIndication)

def sendtrap(event):
    try:
        oid = oidmap[event]
        command = f"snmptrap -v 2c -c {community} {manager} '' {oid}"
        print("Sending Trap: ",command)
        os.system(command)
    except Exception as eer:
        print(eer)
        print("Problem sending Trap for event: ",event)


def healthcheck():
    while True:
        print("Checking System Health")
        total, used, free = shutil.disk_usage("./")
        cpupercent = psutil.cpu_percent(5)
        ramused= psutil.virtual_memory()[2]
        diskfree = int(free/1073741824)
        print('The CPU usage is: ', cpupercent)
        print('RAM memory % used:', ramused)
        print("Disk Free(GB): ",diskfree)
        if cpupercent > cpulimit:
            print("Sending High CPU Usage Trap")
            sendtrap("highcpuusage")
        if ramused > memlimit:
            print("Sending High RAM Usage Trap")
            sendtrap("highmemoryusage")
        if diskfree < dflimit:
            print("Sending Low Disk Space Trap")
            sendtrap("lowdiskspacewarning")
        time.sleep(60)

if __name__ == "__main__":
    hcthread = Thread(target = healthcheck)
    hcthread.start()

    pubsub = r.pubsub()
    pubsub.subscribe(['outgoingsnmpagent'])
    for item in pubsub.listen():
        #print(item)
        if item["type"] == "message":
            #print(pickle.loads(item["data"]))
            data = pickle.loads(item["data"])
            # pprint(data)
            pprint(data)
            sendtrap(data[0])
               