from pprint import pprint
import redis
import pickle
import configparser
import smtplib
from email.mime.text import MIMEText
import email.utils
import pysnmp
from orm import snmpAlerts, session, snmpRules
txtconfig = configparser.ConfigParser()
txtconfig.read('config.ini')

smtpport = txtconfig['EMAIL']['SMTPPORT']
smtpserver = txtconfig['EMAIL']['SMTPSERVER']
smtpusername = txtconfig['EMAIL']['SMTPUSERNAME']
smtppassword = txtconfig['EMAIL']['SMTPPASSWORD']
emailfrom = txtconfig['EMAIL']['FROMADDRESS']
emailsubject = txtconfig['EMAIL']['EMAILSUBJECT']

if txtconfig['REDIS']['PASSWORD']!="":
    r = redis.Redis(
    host=txtconfig['REDIS']['HOST'],
    port=int(txtconfig['REDIS']['PORT']), password=txtconfig['REDIS']['PASSWORD'])
else:
    r = redis.Redis(
    host=txtconfig['REDIS']['HOST'],
    port=int(txtconfig['REDIS']['PORT']))


def sendemail(oid,name,value,toemail,deviceip):
    print("Sending Email:",oid,name,value,toemail,deviceip)
    try:
        s = smtplib.SMTP(smtpserver, int(smtpport))
        s.ehlo()
        s.set_debuglevel(False)
        if s.has_extn('STARTTLS'):
            s.starttls()
            s.ehlo()
            s.login(smtpusername,smtppassword)
    except Exception:
        print("Problem connecting to SMTP")
    msg = MIMEText("Device with IP: %s has generated alarm for OID: %s \nEvent Name: %s\n Event Value:%s" %(deviceip,oid,name,value))
    msg['Subject'] = emailsubject
    msg['From'] = email.utils.formataddr(('emsNEXT Alarm', emailfrom))
    msg['To'] = email.utils.formataddr((toemail, toemail))

    try:

        s.sendmail(emailfrom, [toemail], msg.as_string())
        print("Email to "+toemail+" sent successfully")
        s.close()

    except Exception:
      print("Error sending email to "+toemail)


if __name__ == "__main__":
    
    pubsub = r.pubsub()
    pubsub.subscribe(['nodesnmptraps'])
    for item in pubsub.listen():
        #print(item)
        if item["type"] == "message":
            #print(pickle.loads(item["data"]))
            data = pickle.loads(item["data"])
            # pprint(data)
            for name,val in data[1]:
                print('%s:%s = %s' % (data[0],name.prettyPrint(), val.prettyPrint()))
                snmpalert = snmpAlerts(ip=data[0], alertmsg=name.prettyPrint()+" : "+val.prettyPrint())
                session.add(snmpalert)
                session.commit()

                rulefound = session.query(snmpRules).filter(snmpRules.oid == name.prettyPrint()).first()
                if rulefound and rulefound.sendemail == 1 and int(txtconfig['EMAIL']['SENDEMAIL']) == 1: 
                    print("Sending SNMP Alert email for ip:"+str(data[0]+" OID:"+name.prettyPrint()))
                    sendemail(name.prettyPrint(),rulefound.name, val.prettyPrint(), rulefound.emailrecipients, data[0])


