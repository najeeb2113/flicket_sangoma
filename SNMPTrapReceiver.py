#python snmp trap receiver
from pysnmp.entity import engine, config
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.entity.rfc3413 import ntfrcv
import configparser
snmpEngine = engine.SnmpEngine()
txtconfig = configparser.ConfigParser()
txtconfig.read('config.ini')
import pickle
import redis
if txtconfig['REDIS']['PASSWORD']!="":
    r = redis.Redis(
    host=txtconfig['REDIS']['HOST'],
    port=int(txtconfig['REDIS']['PORT']), password=txtconfig['REDIS']['PASSWORD'])
else:
    r = redis.Redis(
    host=txtconfig['REDIS']['HOST'],
    port=int(txtconfig['REDIS']['PORT']))


TrapAgentAddress=txtconfig['SNMP']['IP'] 
Port=int(txtconfig['SNMP']['PORT'])  
print("Agent is listening SNMP Trap on "+TrapAgentAddress+" , Port : " +str(Port))
print('--------------------------------------------------------------------------')
config.addTransport(
    snmpEngine,
    udp.domainName + (1,),
    udp.UdpTransport().openServerMode((TrapAgentAddress, Port))
)

#Configure community here
config.addV1System(snmpEngine, txtconfig['SNMP']['AREA'], txtconfig['SNMP']['COMMUNITY'])

def cbFun(snmpEngine, stateReference, contextEngineId, contextName,
          varBinds, cbCtx):
          
    print("Received new Trap message");
    execContext = snmpEngine.observer.getExecutionContext(
        'rfc3412.receiveMessage:request'
    )
    for name, val in varBinds:        
        print('%s  %s = %s' % (execContext['transportAddress'], name.prettyPrint(), val.prettyPrint()))
    r.publish(channel="nodesnmptraps", message=pickle.dumps([execContext['transportAddress'][0],varBinds]))
    

ntfrcv.NotificationReceiver(snmpEngine, cbFun)

snmpEngine.transportDispatcher.jobStarted(1)  

try:
    snmpEngine.transportDispatcher.runDispatcher()
except:
    snmpEngine.transportDispatcher.closeDispatcher()
    raise
