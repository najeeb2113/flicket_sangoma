import os
from orm import deviceinfo, session

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer
import configparser
import license
txtconfig = configparser.ConfigParser()
txtconfig.read('config.ini')
def main():
    
    print("|=====================================================|")
    print("| emsNEXT - FTPServer                                 |")
    print("| v1.0.3                                              |")
    print("| Built by Telenext Systems. www.telenextsystems.com  |")
    print("| Please contact samrat@telenextsystems.com           |")
    print("|=====================================================|")

    license.licenseCheck()
    result = session.query(deviceinfo).all()
    authorizer = DummyAuthorizer()
    for user in result:
        if not os.path.isdir(os.path.join(os.getcwd(),"cdrinput",user.device_name)):
            os.mkdir(os.path.join(os.getcwd(),"cdrinput",user.device_name))
        authorizer.add_user(user.device_name, txtconfig['FTPSERVER']['PASSWORD'], os.path.join(os.getcwd(),"cdrinput",user.device_name), perm='elradfmwMT')

    handler = FTPHandler
    handler.authorizer = authorizer
    handler.banner = "telenext ftp server v1.0"
    address = ('', int(txtconfig['FTPSERVER']['PORT']))
    server = FTPServer(address, handler)
    server.max_cons = int(txtconfig['FTPSERVER']['MAXCON'])
    server.max_cons_per_ip = int(txtconfig['FTPSERVER']['MAXCONPERIP'])
    server.serve_forever()

if __name__ == '__main__':
    main()
