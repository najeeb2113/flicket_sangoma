import configparser
import importlib
import os
from os import listdir
from os.path import isfile, join
import xml.etree.ElementTree as ET
import shutil

from orm import cdrParser, session, Base

config = configparser.ConfigParser()
config.read('cdrconfig.ini')




# getting values from config file

myvar=[]
status=[]
for keys in list(config.sections()):
    print(keys)
    try:
        model_list = config[keys]['Tablename']
        the_module = importlib.import_module('orm')  # eg. importlib.import_module('a.b.models')
        the_class = getattr(the_module, model_list)
        # the_class will be the tablename variable parsed orm table object
        # query to test below
        # some_var = the_class.query.first()
        directory = os.getcwd()+config[keys]['Directory']
        directory_contents = os.listdir(directory)
        parameters = config[keys]['Parameters'].split(',')
    except Exception as ftex:
        print("config exception: " + str(ftex))

    # searching for files in directory
    for subdirectories in directory_contents:
        directory = os.getcwd() + config[keys]['Directory']+'/'+subdirectories
        try:
            from os.path import isfile, join
            onlyfiles = [f for f in listdir(directory) if isfile(join(directory, f))]
        except Exception as e:
            print(e)
            onlyfiles =[]
        # print(onlyfiles)
        if len(onlyfiles) > 0:
            for onlyfile in onlyfiles:
                finalfile = os.path.join(directory, onlyfile)
                # print(finalfile)

        # PArsing data from file to db
                tree = ET.parse(finalfile)

                # get root element
                root = tree.getroot()
                allparams={}
                cdrClass = the_class()
                for param in parameters:
                    # print(param)
                    # print(param,':',root.find(param).text)
                    try:
                        paramName= param.split('/')
                        allparams[paramName[1]] = root.find(param).text
                        # getattr(the_class, paramName[1])
                        cdrClass[paramName[1]] = root.find(param).text
                        # print("Param:",param)
                    except:
                        print("Skipping: "+param)
                        allparams[paramName[1]]=""

                # print(allparams)
                cdrClass['device_name'] = subdirectories
                session.add(cdrClass)
                session.commit()

                # ins = the_class(device_name=subdirectories, direction=allparams['direction'], uuid=allparams['uuid'],
                #            sip_from_user=allparams['sip_from_user'],sip_trunk_name=allparams['sip_trunk_name'],hangup_cause=allparams['hangup_cause'],
                #            hangup_cause_q850=allparams['hangup_cause_q850'],
                #            duration=allparams['duration'],
                #            sip_received_ip=allparams['sip_received_ip'],
                #            sip_to_host=allparams['sip_to_host'],
                #            sip_use_codec_name=allparams['sip_use_codec_name'],
                #            endpoint_disposition=allparams['endpoint_disposition'],
                #            )
                # session.add(ins)
                # session.commit()

            # Backup the file to given directory ,check and create directory
                try:
                    print("in backup")
                    backup_directory = os.getcwd() + config[keys]['BACKUPDIRECTORY'] + '/' + subdirectories

                    if os.path.exists(backup_directory):
                        print("availabel path for backupdevice ************")
                        shutil.move(os.path.join(directory, onlyfile), os.path.join(backup_directory, onlyfile))
                    else:
                        os.mkdir(backup_directory)
                        print('device path created *******')
                        shutil.move(os.path.join(directory, onlyfile), os.path.join(backup_directory, onlyfile))
                    status.append("success"+str(directory))
                except Exception as ftex:
                    print("config exception: " + str(ftex))
        else:
            myvar.append("no files in "+str(directory))
            print("no files in ",directory)

