import configparser
import json
import dash
from dash import dcc
from dash import html
# import dash_core_components as dcc
# import dash_html_components as html
import plotly.express as px
import pandas as pd
from flask import redirect,request
from flask_login import login_required

from orm import session, cdrParser, vega
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc


def create_dash_application(flask_app):
    dash_app = dash.Dash(server=flask_app, name="Dashboard", url_base_pathname="/dash/",
                         external_stylesheets=[dbc.themes.BOOTSTRAP])

    @flask_app.route('/performance/graphs/')
    def performo():
        alldata = []
        devicedata = session.query(vega).order_by(vega.Time_created.desc()).limit(1000).all()
        session.commit()

        deviceconfig = configparser.ConfigParser()
        deviceconfig.read('deviceconfig.ini')
        graph_all_param = {}
        for a in devicedata:
            # print(a.device_type)
            graphparam = []
            try:
                # checking graph column in config if no goto except
                graph = deviceconfig[a.device_type]['graph'].split(',')
                # print(graph)
                for item in graph:
                    # print(item)
                    graphparam.append(item.split('-'))
                graph_all_param[a.device_type] = graphparam
                # print(graph_all_param,'\n')

                # generating data to store in pandas dataframe
                # all paramvalues,date,devicename & devicetype which has graph column is stored in alldata
                # alldata is passed into dataframe
                final_dictionary = json.loads(a.paramvalue)
                final_dictionary["date"] = str(a.Time_created.date())
                final_dictionary["device_name"] = str(a.device_name)
                final_dictionary['device_type'] = str(a.device_type)
                alldata.append(final_dictionary)
                # print(final_dictionary)
            except:
                print("no graphs for {}".format(a.device_type))

        df = pd.DataFrame(alldata)
        # df=df.drop_duplicates(subset=['date', 'device_name'],keep="last")

        unique_dates = df.date.unique()
        device_names = df.device_name.unique()
        device_types = df.device_type.unique()
        graphs = []
        for type in device_types:
            temptest = df.loc[df['device_type'] == type]
            for i in graph_all_param[type]:
                # print(i)
                if i[1] == "line":
                    dfc = temptest[temptest[i[0]].notna()]
                    dfc = dfc.sort_values(by=i[0])
                    graphs.append(dbc.Col(dcc.Graph(
                        id='graph-{}'.format(i[0]),
                        figure=px.line(dfc, x="date", y=i[0], color="device_name", title="{}-{}".format(type, i[0]))
                    ), width="6"))

                else:
                    dfc = temptest[temptest[i[0]].notna()]
                    dfc = dfc.sort_values(by=i[0])
                    graphs.append(dbc.Col(dcc.Graph(
                        id='graph-{}'.format(i[0]),
                        figure=px.bar(dfc, x="date", y=i[0], color="device_name", barmode="group",
                                      title="{}-{}".format(type, i[0]))
                    ), width="6"))

        dash_app.layout = html.Div(children=[

            html.Div(
                style={"backgroundColor": "#f5f6f8", "paddingTop": "20px", "paddingBottom": "20px",
                       "paddingLeft": "38px",
                       "paddingRight": "40px"}, children=[
                    html.H2(children='Performance Graph'),

                    html.Div(id='container', children=[
                        dbc.Row(graphs, align="center", className="gy-5"),
                    ]),

                ]),
        ])

        for view_function in dash_app.server.view_functions:
            if view_function.startswith(dash_app.config.url_base_pathname):
                dash_app.server.view_functions[view_function] = login_required(
                    dash_app.server.view_functions[view_function]
                )
        myhost = request.host.split(':')
        # if myhost[0] == '127.0.0.1' or myhost[0] == 'localhost' and not request.is_secure:
        #     return redirect('/dash/')
        # else:
        #     return redirect('https://'+request.host+'/dash/', code=302)
        return redirect('/dash/')

    # *&%^$$%^&*
    alldata = []
    devicedata = session.query(vega).all()
    session.commit()

    deviceconfig = configparser.ConfigParser()
    deviceconfig.read('deviceconfig.ini')
    graph_all_param = {}
    for a in devicedata:
        # print(a.device_type)
        graphparam = []
        try:
            # checking graph column in config if no goto except
            graph = deviceconfig[a.device_type]['graph'].split(',')
            # print(graph)
            for item in graph:
                # print(item)
                graphparam.append(item.split('-'))
            graph_all_param[a.device_type] = graphparam
            # print(graph_all_param,'\n')

            # generating data to store in pandas dataframe
            # all paramvalues,date,devicename & devicetype which has graph column is stored in alldata
            # alldata is passed into dataframe
            final_dictionary = json.loads(a.paramvalue)
            final_dictionary["date"] = str(a.Time_created.date())
            final_dictionary["device_name"] = str(a.device_name)
            final_dictionary['device_type'] = str(a.device_type)
            alldata.append(final_dictionary)
            # print(final_dictionary)
        except:
            print("no graphs for {}".format(a.device_type))

    df = pd.DataFrame(alldata)
    # df=df.drop_duplicates(subset=['date', 'device_name'],keep="last")

    unique_dates = df.date.unique()
    device_names = df.device_name.unique()
    device_types = df.device_type.unique()
    graphs = []
    for type in device_types:
        # print(graph_all_param[type])
        temptest = df.loc[df['device_type'] == type]
        # device_names = temptest.device_name.unique()
        for i in graph_all_param[type]:
            # print(i)
            if i[1] == "line":
                dfc = temptest[temptest[i[0]].notna()]
                dfc = dfc.sort_values(by=i[0])
                graphs.append(dbc.Col(dcc.Graph(
                    id='graph-{}'.format(i[0]),
                    figure=px.line(dfc, x="date", y=i[0], color="device_name", title="{}-{}".format(type, i[0]))
                ), width="6"))
                # text = i[0],
            else:
                dfc = temptest[temptest[i[0]].notna()]
                dfc = dfc.sort_values(by=i[0])
                graphs.append(dbc.Col(dcc.Graph(
                    id='graph-{}'.format(i[0]),
                    figure=px.bar(dfc, x="date", y=i[0], color="device_name", barmode="group",
                                  title="{}-{}".format(type, i[0]))
                ), width="6"))

    dash_app.layout = html.Div(children=[

        html.Div(
            style={"backgroundColor": "#f5f6f8", "paddingTop": "20px", "paddingBottom": "20px", "paddingLeft": "38px",
                   "paddingRight": "40px"}, children=[
                html.H2(children='Performance Graph'),

                # html.Div(children="Device Data : CPU, Memory".join("{}".format(i[0]) for i in graphparam)),

                # html.Div(id='container' ,children=graphs),
                html.Div(id='container', children=[
                    dbc.Row(graphs, align="center", className="gy-5"),
                ]),


            ]),
    ])


    for view_function in dash_app.server.view_functions:
        if view_function.startswith(dash_app.config.url_base_pathname):
            dash_app.server.view_functions[view_function] = login_required(
                dash_app.server.view_functions[view_function]
            )

    # return dash_app
