import configparser
import json
import dash
from dash import dcc
from dash import html
# import dash_core_components as dcc
# import dash_html_components as html
import plotly.express as px
import pandas as pd
from flask import redirect, request
from flask_login import login_required

from orm import session, cdrParser, vega, mosScores
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc


def create_dash_application2(flask_app):
    dash_app = dash.Dash(server=flask_app, name="MosReports", url_base_pathname="/diagnostics/",
                         external_stylesheets=[dbc.themes.BOOTSTRAP])

    @flask_app.route('/diagnostics/graphs/')
    def hello():
        alldata = []
        result2 = session.query(mosScores).all()
        session.commit()

        for a in result2:
            # print(a)
            final_dictionary = {}
            try:
                # generating data to store in pandas dataframe

                final_dictionary["date"] = str(a.timestamp)
                final_dictionary["score"] = str(a.score)
                final_dictionary['ip'] = str(a.ip)
                alldata.append(final_dictionary)
                # print(final_dictionary)
            except:
                print("no graphs for {}".format(a.device_type))

        df = pd.DataFrame(alldata)
        # df=df.drop_duplicates(subset=['date', 'ip'],keep="last")
        # unique_dates = df.date.unique()

        graphs = []
        try:
            dfc = df.sort_values(by=['score'])
            graphs.append(dbc.Col(dcc.Graph(
                id='graph',
                figure=px.line(dfc, x="date", y='score', color="ip", title="Mos - Report")
            ), width="12"))
        except:
            graphs.append(dbc.Col(html.H3(children='No Data Available')))

        dash_app.layout = html.Div(children=[

            html.Div(
                style={"backgroundColor": "#f5f6f8", "paddingTop": "20px", "paddingBottom": "20px",
                       "paddingLeft": "38px",
                       "paddingRight": "40px"}, children=[
                    html.H2(children='MOS-Reports Graph'),
                    html.Div(id='container', children=[
                        dbc.Row(graphs, align="center", className="gy-5"),
                    ]),

                ]),
        ])

        for view_function in dash_app.server.view_functions:
            if view_function.startswith(dash_app.config.url_base_pathname):
                dash_app.server.view_functions[view_function] = login_required(
                    dash_app.server.view_functions[view_function]
                )
        myhost = request.host.split(':')
        # if myhost[0] == '127.0.0.1' or myhost[0] == 'localhost' and not request.is_secure:
        #     return redirect('/diagnostics/')
        # else:
        #     return redirect('https://' + request.host + '/diagnostics/', code=302)
        return redirect('/diagnostics/')

    alldata = []
    result2 = session.query(mosScores).all()
    session.commit()

    for a in result2:
        # print(a)
        final_dictionary = {}
        try:
            # generating data to store in pandas dataframe

            final_dictionary["date"] = str(a.timestamp)
            final_dictionary["score"] = str(a.score)
            final_dictionary['ip'] = str(a.ip)
            alldata.append(final_dictionary)
            # print(final_dictionary)
        except:
            print("no graphs for {}".format(a.device_type))

    df = pd.DataFrame(alldata)
    # df=df.drop_duplicates(subset=['date', 'ip'],keep="last")
    # unique_dates = df.date.unique()

    graphs = []
    try:
        dfc = df.sort_values(by=['score'])
        graphs.append(dbc.Col(dcc.Graph(
            id='graph',
            figure=px.line(dfc, x="date", y='score', color="ip", title="Mos - Report")
        ), width="12"))
    except:
        graphs.append(dbc.Col(html.H3(children='No Data Available')))

    dash_app.layout = html.Div(children=[

        html.Div(
            style={"backgroundColor": "#f5f6f8", "paddingTop": "20px", "paddingBottom": "20px", "paddingLeft": "38px",
                   "paddingRight": "40px"}, children=[
                html.H2(children='MOS-Reports Graph'),
                html.Div(id='container', children=[
                    dbc.Row(graphs,align="center",className="gy-5"),
                ]),

            ]),
    ])

    for view_function in dash_app.server.view_functions:
        if view_function.startswith(dash_app.config.url_base_pathname):
            dash_app.server.view_functions[view_function] = login_required(
                dash_app.server.view_functions[view_function]
            )
