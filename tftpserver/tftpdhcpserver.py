from dhcpserver import dhcpdinitialize
from tftpserver import tftpdinitialize
import threading
import sys,os
import license


if __name__ == "__main__":
    
    print("|=====================================================|")
    print("| emsNEXT - Robust DHCP and TFTP Server               |")
    print("| v 0.1.7                                             |")
    print("| Built by Telenext Systems. www.telenextsystems.com  |")
    print("| Please contact samrat@telenextsystems.com           |")
    print("|=====================================================|")

    license.licenseCheck()
    print("Starting DHCP Server")
    x = threading.Thread(target=dhcpdinitialize)
    x.start()

    print("Starting TFTP Server")
    y = threading.Thread(target=tftpdinitialize)
    y.start()


