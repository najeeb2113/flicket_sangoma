import configparser
from pprint import pprint
from flask import Blueprint, render_template, request,redirect, url_for , send_from_directory , session , flash, request
import random
import string
import os
import datetime
from flask_login import login_required, current_user
from orm import session, deviceinfo, firmwarefiles, firmwareupdatejob, Auditlogs
import pprint

fu_bp = Blueprint('firmwareupdater', __name__,)

subcdr_menu = []
api_key = ""
proxy_url = ""

config = configparser.ConfigParser()
deviceconfig = configparser.ConfigParser()
cdrconfig = configparser.ConfigParser()


@fu_bp.before_request
def before_request():
    myurlhost = 'https://' + request.host

    config.read('config.ini')
    deviceconfig.read('deviceconfig.ini')
    cdrconfig.read('cdrconfig.ini')

    api_key = config['API']['KEY']
    proxy_url = config['PROXY']['URL']
    
    if not subcdr_menu:
        for keys in list(cdrconfig.sections()):
            print("mySUbmenu's:", keys)
            subcdr_menu.append(keys)


@fu_bp.route('/fwu')
@login_required
def fwindex():
    return "This is an example FU app"


@fu_bp.route('/fwu/viewjobs')
@login_required
def fwviewjobs():
    jobs = session.query(firmwareupdatejob).all()
    return render_template("fwu/viewjobs.html",subcdr_menu=subcdr_menu,show50="show",active_var50='active',active_var51='active',jobs=jobs)


@fu_bp.route('/fwu/newjob')
@login_required
def fwnewjob():
    if current_user.isadmin == "true":
        devicedata = session.query(deviceinfo).all()
    else:
        devicedata = session.query(deviceinfo).filter(deviceinfo.region == current_user.region).all()
    allfiles = session.query(firmwarefiles).all()
    return render_template("fwu/newjob.html",subcdr_menu=subcdr_menu,show50="show",active_var50='active',active_var52='active',files=allfiles,devices=devicedata)

@fu_bp.route('/fwu/viewfiles')
@login_required
def fwviewfiles():
    fwfiles = session.query(firmwarefiles).all()
    return render_template("fwu/viewfiles.html",subcdr_menu=subcdr_menu,show50="show",active_var50='active',active_var53='active',files=fwfiles)


@fu_bp.route('/fwu/newfile')
@login_required
def fwnewfile():
    return render_template("fwu/newfile.html",subcdr_menu=subcdr_menu,show50="show",active_var50='active',active_var54='active')

@fu_bp.route('/fwu/viewjobdetail/<jobid>')
@login_required
def fwviewjobdetail(jobid):
    return render_template("viewjobdetail.html",subcdr_menu=subcdr_menu,show50="show",active_var50='active',active_var51='active',job={},progress=1)


@fu_bp.route('/fwu/viewactivitylog')
@login_required
def fwlog():
    lines = fwtail('activity.log',100)
    return render_template('viewactivitylog.html',subcdr_menu=subcdr_menu,show50='show',active_var55 ='active', content=lines)


@fu_bp.route('/fwu/fileupload',methods = ['POST', 'GET'])
def fwfileupload():
    pprint.pprint(request.form["name"])
    if request.method == 'POST':
        if 'file' not in request.files:
            print("File is missing")
            fwfile = ""
        else:
            fwfile = request.files['file']
        if fwfile.filename == '':
            print("Filename Missing...Adding filename")
            filename = fwrandomword() + ".tgz"
            return redirect(request.url)
        if fwfile :
            filename = fwfile.filename
            fwfile.save(os.path.join("./firmwares", filename))
            name = request.form["name"]
            # description = request.form["description"]
            version = request.form["version"]
            devicetype = request.form["devicetype"]
            if name != "" and devicetype != "" and version != "":
                now = datetime.datetime.now()
                fwf = firmwarefiles(name=name, version=version, type=devicetype, filename=filename)#DB Insert
                session.add(fwf)
                session.commit()

                # Activity LOG AUDIT
                ins2 = Auditlogs(devicename=devicetype,
                                 action="Firmware file with file name {} and version {} for devicetype {} is uploaded into ems by {}.".format(
                                     filename, version, devicetype, current_user.fullname))
                session.add(ins2)
                session.commit()
                flash("File Uploaded Successfully")
                return redirect("/fwu/viewfiles")
            else:
                flash("Error Missing Fields")
                return redirect("/fwu/viewfiles")


@fu_bp.route('/fwu/newjobpost',methods=['POST'])
def fwnewjobpost():
    name = request.form.get("name")
    firmware = request.form.get("firmware")
    sbcs = request.form.getlist("sbcselect")

    print(name,firmware,sbcs)

    if name is None or firmware is None or sbcs is None:
        return redirect('/fwu/newjob/Error.All+params+required')
    else:
        sbclist= []
        
        for sbcid in sbcs:
            print(sbcid)
            device = session.query(deviceinfo).filter(deviceinfo.id == int(sbcid)).first()
            firmwareupdate = firmwareupdatejob(name=name, devicename=device.device_name, 
            deviceip=device.ip, devicetype=device.device_type, firmware=firmware,
            targetversion="NEW", status="READY",username=device.username, password=device.password)
            session.add(firmwareupdate)
            session.commit()

            firmwarefiledetails = session.query(firmwarefiles).filter(firmwarefiles.filename == firmware).first()

            # Activity LOG AUDIT
            ins2 = Auditlogs(devicename=device.device_name,ip=device.ip, action="Firmware update with file name {} and version {} started for {} by {}.".format(firmware,firmwarefiledetails.version,device.device_name,current_user.fullname))
            session.add(ins2)
            session.commit()
        return redirect('/fwu/viewjobs')
#End of post routes





#All action routes
@fu_bp.route('/fwu/getfile/<filename>')
@login_required
def fwuploaded_file(filename):
    return send_from_directory("./firmwares",
                               filename)


@fu_bp.route('/fwu/pause/<jobid>/<redtype>')
@login_required
def fwpause(jobid,redtype):

    session.query(firmwareupdatejob).filter_by(id=jobid).update(dict(status='Paused'))
    session.commit()

    # db.updatejobs.update_one({
    #     '_id': ObjectId(jobid)
    # }, {
    #     '$set': {
    #         'status': 'Paused'
    #     }
    # }, upsert=False)
    if redtype == 'd':
        return redirect('/fwu/viewjobdetail/'+jobid)
    else:
        return redirect('/fwu/viewjobs')


@fu_bp.route('/fwu/resume/<jobid>/<redtype>')
@login_required
def fwresume(jobid,redtype):
    session.query(firmwareupdatejob).filter_by(id=jobid).update(dict(status='Pending'))
    session.commit()

    # db.updatejobs.update_one({
    #     '_id': ObjectId(jobid)
    # }, {
    #     '$set': {
    #         'status': 'Pending'
    #     }
    # }, upsert=False)
    if redtype == 'd':
        return redirect('/fwu/viewjobdetail/'+jobid)
    else:
        return redirect('/fwu/viewjobs')


@fu_bp.route('/fwu/filedelete/<fileid>')
@login_required
def fwfiledelete(fileid):
    print("filedelete "+fileid)

    firmfile = session.query(firmwarefiles).filter(firmwarefiles.id == int(fileid)).first()
    # Activity LOG AUDIT
    ins2 = Auditlogs(devicename=firmfile.type,
                     action="Firmware file with file name {} and version {} for devicetype {} is removed by {}.".format(
                         firmfile.filename, firmfile.version, firmfile.type, current_user.fullname))
    session.add(ins2)
    session.commit()

    # file = db.firmwares.find_one({'_id':ObjectId(fileid)})
    # filename = file["filename"]
    # db.firmwares.delete_one({'_id': ObjectId(fileid)})
    os.remove(os.path.join("./firmwares", firmfile.filename))

    session.query(firmwarefiles).filter(firmwarefiles.id == int(fileid)).delete()
    session.commit()


    return "success"


@fu_bp.route('/fwu/jobdelete/<jobid>')
@login_required
def fwjobdelete(jobid):
    print("jobdelete "+jobid)
    session.query(firmwareupdatejob).filter(firmwareupdatejob.id == int(jobid)).delete()
    session.commit()

    # db.updatejobs.delete_one({'_id':ObjectId(jobid)})

    return "success"

#End of action routes


#Other Support functions
def fwrandomword():
   return ''.join(random.choice(string.ascii_lowercase) for i in range(10))


def fwtail(filename, lines=1, _buffer=4098):
    """Tail a file and get X lines from the end"""
    # place holder for the lines found
    f = open(filename,'rb')
    lines_found = []

    # block counter will be multiplied by buffer
    # to get the block size from the end
    block_counter = -1

    # loop until we find X lines
    while len(lines_found) < lines:
        try:
            f.seek(block_counter * _buffer, os.SEEK_END)
        except IOError:  # either file is too small, or too many lines requested
            f.seek(0)
            lines_found = f.readlines()
            break

        lines_found = f.readlines()

        # we found enough lines, get out
        # Removed this line because it was redundant the while will catch
        # it, I left it for history
        # if len(lines_found) > lines:
        #    break

        # decrement the block counter to get the
        # next X bytes
        block_counter -= 1

    return lines_found[-lines:]