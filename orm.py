from sqlalchemy import Column, ForeignKey, Integer, String, BigInteger, TIMESTAMP, null, text, Boolean, TEXT, TIME, DATE, \
    Float, DateTime, func
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, scoped_session
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import configparser
from flask_login import UserMixin


config = configparser.ConfigParser()
config.read('config.ini')

dbhost = config['DATABASE']['IP']
dbuser = config['DATABASE']['USERNAME']
dbpass = config['DATABASE']['PASSWORD']
dbname = config['DATABASE']['DB']
dbport = int(config['DATABASE']['PORT'])
Base = declarative_base()



class Subscriber(Base,UserMixin):
    __tablename__='admin'
    id = Column(BigInteger, primary_key=True)
    fullname = Column(String(50), nullable=False)
    email = Column(String(50), nullable=False, unique=True)
    password = Column(String(150), nullable=False)
    mobile = Column(String(20), nullable=False)
    region = Column(String(30), nullable=True)
    readonly = Column(String(20), nullable=True)
    logged_in = Column(String(20), nullable=True)
    session_id = Column(String(150), nullable=True)
    last_loggedintime = Column(DateTime, nullable=True)
    isadmin = Column(String(20), nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class vega(Base):
    __tablename__ = 'devicedata'
    id = Column(BigInteger, primary_key=True ,autoincrement=True)
    Time_created = Column(DateTime(timezone=True), server_default=func.now())
    device_name = Column(String(30), nullable=True)
    device_type = Column(String(30), nullable=True)
    paramname = Column(String(250),nullable=True)
    paramvalue = Column(String(10000), nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class DeviceLicenses(Base):
    __tablename__ = 'devicelicense'
    id = Column(BigInteger, primary_key=True ,autoincrement=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    device_name = Column(String(30), nullable=True)
    device_type = Column(String(30), nullable=True)
    deviceip = Column(String(90),nullable=True)
    backupfilepath = Column(String(400), nullable=True)
    description = Column(String(550), nullable=True)
    name = Column(String(90), nullable=True)
    email = Column(String(90), nullable=True)
    product = Column(String(90), nullable=True)
    maxcalls = Column(String(90), nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class Devicebackups(Base):
    __tablename__ = 'devicebackups'
    id = Column(BigInteger, primary_key=True ,autoincrement=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    device_name = Column(String(30), nullable=True)
    device_type = Column(String(30), nullable=True)
    deviceip = Column(String(90),nullable=True)
    backupfilepath = Column(String(400), nullable=True)
    filename = Column(String(90),nullable=True)
    description = Column(String(550), nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)


class Triggers(Base):
    __tablename__ = 'triggers'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    oid = Column(String(80), nullable=True)
    priority = Column(String(30), nullable=True)
    action = Column(String(30), nullable=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)

class Alarms(Base):
    __tablename__ = 'alarms'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    ip = Column(String(90), nullable=True)
    priority = Column(String(30), nullable=True)
    payload = Column(String(510), nullable=True)
    emailflag = Column(Boolean, nullable=True)
    apiflag = Column(Boolean, nullable=True)
    popupflag = Column(Boolean, nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)



class deviceinfo(Base):
    __tablename__ = 'deviceinfo'
    id = Column(BigInteger, primary_key=True)
    ip = Column(String(80), nullable=True)
    device_type = Column(String(30), nullable=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    device_name = Column(String(30), nullable=True, unique=True)
    username = Column(String(30), nullable=True)
    password = Column(String(30), nullable=True)
    location = Column(String(30), nullable=True)
    region = Column(String(30), nullable=True)
    description = Column(String(100), nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)

class cdrParser(Base):
    __tablename__ = 'cdrparser'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    device_name = Column(String(80), nullable=True)
    uuid = Column(String(80), nullable=True)
    sip_from_user = Column(String(80), nullable=True)
    sip_trunk_name = Column(String(80), nullable=True)
    direction = Column(String(80), nullable=True)
    hangup_cause= Column(String(80), nullable=True)
    hangup_cause_q850= Column(String(80), nullable=True)
    duration= Column(String(80), nullable=True)
    sip_received_ip= Column(String(80), nullable=True)
    sip_to_host= Column(String(80), nullable=True)
    sip_use_codec_name= Column(String(80), nullable=True)
    endpoint_disposition= Column(String(80), nullable=True)

    def __unicode__(self):
        return str(self.id)

    def __repr__(self):
        return str(self.id)

    def __getitem__(self, item):
        return getattr(self, item)

    def __setitem__(self, item, value):
        return setattr(self, item, value)


class monitoringNodes(Base):
    __tablename__ = 'monitoringnodes'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    name= Column(String(80), nullable=True)
    ip= Column(String(30), nullable=True)
    description = Column(String(300), nullable=True)

class mosScores(Base):
    __tablename__ = 'mosscores'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    ip= Column(String(30), nullable=False)
    score = Column(String(10), nullable=False)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())


class snmpAlerts(Base):
    __tablename__ = 'snmpalerts'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    ip= Column(String(30), nullable=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())
    alertmsg = Column(String(1024), nullable=True)

class snmpRules(Base):
    __tablename__ = 'snmprules'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    oid = Column(String(30), nullable=True)
    name = Column(String(30), nullable=True)
    emailrecipients =  Column(String(100), nullable=True)
    sendemail = Column(Integer, nullable=True)
    sendwebnotification = Column(Integer, nullable=True)
    forwardonsnmp = Column(Integer, nullable=True)

class firmwarefiles(Base):
    __tablename__ = 'firmwarefiles'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    name = Column(String(100), nullable=True)
    filename = Column(String(100), nullable=True)
    version = Column(String(30), nullable=True)
    type = Column(String(10), nullable=True)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())

class firmwareupdatejob(Base):
    __tablename__ = 'firmwareupdatejobs'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    name = Column(String(30), nullable=True)
    devicename = Column(String(30), nullable=True)
    deviceip = Column(String(60), nullable=True)
    devicetype = Column(String(10), nullable=True)
    firmware = Column(String(200), nullable=True)
    targetversion = Column(String(20), nullable=True)
    status = Column(String(30), nullable=True)
    username = Column(String(30), nullable=True)
    password = Column(String(30), nullable=True)
    ts = Column(DateTime(timezone=True), server_default=func.now())



class Auditlogs(Base):
    __tablename__ = 'auditlogs'
    id = Column(BigInteger, primary_key=True,autoincrement=True)
    action = Column(String(600), nullable=True)
    devicename = Column(String(30), nullable=True)
    ip = Column(String(60), nullable=True)
    ts = Column(DateTime(timezone=True), server_default=func.now())


engine = create_engine('mysql+pymysql://{}:{}@{}:{}/{}'.format(config['DATABASE']['USERNAME'],config['DATABASE']['PASSWORD'],config['DATABASE']['IP'],config['DATABASE']['PORT'],config['DATABASE']['DB']),pool_recycle=280,isolation_level="READ UNCOMMITTED",pool_size=20, max_overflow=0)
Session = sessionmaker(bind=engine)
session = scoped_session(Session)
Base.metadata.create_all(engine)
Base.query = session.query_property()
