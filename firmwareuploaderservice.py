import datetime
import json
import time
import warnings
import pprint
import os

from flask_login import current_user

from orm import vega, session, firmwareupdatejob, Auditlogs
from selenium import webdriver
import configparser
from selenium.webdriver.chrome.service import Service
import random
from selenium.webdriver.chrome.options import Options
import os
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
config = configparser.ConfigParser()
config.read('deviceconfig.ini')
sysconfig = configparser.ConfigParser()
sysconfig.read('config.ini')

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("window-size=1200x600")
ser = Service(os.getcwd() + sysconfig["DRIVERPATH"]["CHROMEPATH"])
#chrome_driver = os.getcwd() + "/chromedriver.exe"

result = session.query(firmwareupdatejob).all()
session.commit()

# Activity LOG AUDIT
ins2 = Auditlogs(devicename='Upgrade Firmware',
                 action="Firmware update service.py was executed at {}.".format(datetime.datetime.now()))
session.add(ins2)
session.commit()
for data in result:
    if data.status == "READY" or data.status == "Pending":
        if data.devicetype == "SBC":
            session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='STARTED'))
            session.commit()
            driver = webdriver.Chrome(service=ser, options=chrome_options)
            driver.set_page_load_timeout(float(sysconfig["DRIVERPATH"]["TIMEOUT"]))
            #driver = webdriver.Chrome(options=chrome_options, executable_path=chrome_driver)
            try:
                driver.get(data.deviceip)
                driver.find_element(
                    By.XPATH, config[data.devicetype]['username_id']).send_keys(data.username)
                form = driver.find_element(
                    By.XPATH, config[data.devicetype]['password_id']).send_keys(data.password)
                driver.find_element(
                    By.XPATH, config[data.devicetype]['password_id']).send_keys(Keys.RETURN)
                print("login :done")
                driver.get_screenshot_as_file("capturelogin.png")
                url = data.deviceip + 'SAFe/sng_update_manager_alias'
                driver.get(url)
                print("firmware update URL Open")
                time.sleep(3)
                driver.get_screenshot_as_file("capture.png")
                try:
                    element = driver.find_element(By.XPATH, '/html[1]/body[1]/div[1]/div[4]/div[1]/form[1]/a[1]/span[1]')
                    element.click()
                    time.sleep(1)
                    driver.get_screenshot_as_file("capture1.png")
                    print("clicked upload button")
                except:
                    print("exception occured while uploading a file.")
                try:
                    print("trying to push file into the upload file box")
                    driver.implicitly_wait(5)
                    elm = driver.find_element(By.XPATH, '/html[1]/body[1]/div[4]/div[2]/table[1]/tbody[1]/tr[1]/td[2]/form[1]/input[1]')
                    print(os.getcwd()+'/firmwares/'+data.firmware)
                    elm.send_keys(os.getcwd()+'/firmwares/'+data.firmware)

                    driver.get_screenshot_as_file("capture2.png")
                except:
                    # doesnt matter
                    print("upload file failed")
                try:
                    driver.implicitly_wait(5)
                    element = driver.find_element(By.XPATH,"/html/body/div[4]/div[3]/div/button[1]")
                    element.click()
                    time.sleep(3)
                    driver.get_screenshot_as_file("capturelast.png")
                    print("clicked final button")

                    session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='Complete'))
                    session.commit()
                except:
                    session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='READY'))
                    session.commit()
                    driver.get_screenshot_as_file("capturelast.png")
                    print("uploading failed at last section")

            except Exception as erm:
                session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='READY'))
                session.commit()
                print("Exception detected for IP: "+data.deviceip +
                      ". Check config file. Message: "+str(erm))

            driver.implicitly_wait(3)
            driver.quit()
        else:
            session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='STARTED'))
            session.commit()
            # driver = webdriver.Chrome(service=ser, options=chrome_options)
            print("type : vega")
            driver = webdriver.Chrome(service=ser, options=chrome_options)
            driver.set_page_load_timeout(float(sysconfig["DRIVERPATH"]["TIMEOUT"]))
            try:
                driver.get(data.deviceip)
                driver.find_element(
                    By.XPATH, config[data.devicetype]['username_id']).send_keys(data.username)
                form = driver.find_element(
                    By.XPATH, config[data.devicetype]['password_id']).send_keys(data.password)
                driver.find_element(
                    By.XPATH, config[data.devicetype]['password_id']).send_keys(Keys.RETURN)
                print("login :done")
                driver.get_screenshot_as_file("captureloginvega.png")

                url = data.deviceip + 'aupgrade.htm'
                driver.get(url)
                print("firmware update URL Open")
                time.sleep(3)
                driver.get_screenshot_as_file("capturevega.png")
                try:
                    print("trying to push file into the upload file box vega")
                    driver.implicitly_wait(3)
                    elm = driver.find_element(By.XPATH,'//*[@id="upload_form"]/span/input')
                    print(os.getcwd() + '/firmwares/' + data.firmware)
                    elm.send_keys(os.getcwd() + '/firmwares/' + data.firmware)
                    driver.get_screenshot_as_file("capturevega2.png")
                except:
                    # doesnt matter
                    print("upload file path allot failed")
                try:
                    driver.implicitly_wait(5)
                    element = driver.find_element(By.XPATH, '//*[@id="upload_form"]/input[4]')
                    element.click()
                    time.sleep(3)
                    driver.get_screenshot_as_file("capturelastvega.png")
                    print("clicked final button")

                    session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='Complete'))
                    session.commit()
                except:
                    driver.get_screenshot_as_file("capturelastvega.png")
                    session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='READY'))
                    session.commit()
                    print("uploading failed at last section")

            except Exception as erm:
                session.query(firmwareupdatejob).filter_by(id=data.id).update(dict(status='READY'))
                session.commit()
                print("Exception detected for IP: " + data.deviceip +
                      ". Check config file. Message: " + str(erm))

            driver.implicitly_wait(3)
            driver.quit()

print("Ending service")
